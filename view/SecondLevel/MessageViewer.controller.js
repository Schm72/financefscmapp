jQuery.sap.require("com.springer.financefscmapp.util.Formatter");
jQuery.sap.require("com.springer.financefscmapp.util.Controller");

com.springer.financefscmapp.util.Controller.extend("com.springer.financefscmapp.view.SecondLevel.MessageViewer", {
	
	sMessageURL: "",
	i18model: {},

	onInit: function() {
		this.getRouter().attachRoutePatternMatched(this.onRouteMatched, this);
	
		this.getView().addEventDelegate({
			onAfterShow: jQuery.proxy(function(evt) {
				this.onAfterShow(evt);
			}, this)
		});
	},

	onRouteMatched: function(evt) {
		var oParameters = evt.getParameters();
		// check that the called view name is like expected
		if (oParameters.name !== "_MessageViewer") {
			return;
		}
		this.sMessageURL = oParameters.arguments.messageid;
		this.i18model = this.getView().getModel("i18n").getResourceBundle();

		var UserPreferences = sap.ui.getCore().getModel("UserPreferences");
		var bReplace = sap.ui.Device.system.phone ? false : true;
		if(typeof UserPreferences === "undefined") {
			console.log("Redirect to start");
			this.getRouter().navTo("_A1_firstVisitCheck", {
				currentView: this.getView()
			}, bReplace);
			return;
		}
	},
	onAfterShow: function() {
		var messageContainer = this.getView().byId("MessageTextContent");
		var departmentContainer = this.getView().byId("DepartmentContainerId");
		var categoryContainer = this.getView().byId("CategoryContainerId");
		var categoryIconId = this.getView().byId("CategoryIconId");
        var oModel = this.getView().getModel();
		oModel.read("/"+this.sMessageURL, null, null, true,
			function(oData) {
				messageContainer.setValue(oData.Message);
				if( oData.Department !== "") {
					departmentContainer.setText( com.springer.financefscmapp.util.Formatter.getDepartmentTXT( oData.Department ) );
				} else {
					departmentContainer.setText(oData.Origin);
				}
				if( oData.Category !== "") {
					categoryContainer.setText( com.springer.financefscmapp.util.Formatter.getCaegoryTXT( oData.Category ) );
					categoryContainer.setVisible( true );
					categoryIconId.setVisible( false );
				} else {
					categoryContainer.setVisible( false );
					categoryIconId.setVisible( true );
					categoryIconId.setIcon( com.springer.financefscmapp.util.Formatter.MessageStateIcon( oData.MessageType ) );
					categoryIconId.setState( com.springer.financefscmapp.util.Formatter.MessageState( oData.MessageType ) );
				}
			},
			function() {
				messageContainer.setValue("No message found ...");
			}
		);
	},

	handleNavToHome: function() {
		var bReplace = sap.ui.Device.system.phone ? false : true;
		this.getRouter().navTo("_A2_Welcome", {
			currentView: this.getView()
		}, bReplace);
	},
	onNavBack: function() {
		var bReplace = sap.ui.Device.system.phone ? false : true;
		this.getRouter().navTo("_MessageCollector", {
			currentView: this.getView()
		}, bReplace);
	}

});