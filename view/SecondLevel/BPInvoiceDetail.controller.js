jQuery.sap.require("com.springer.financefscmapp.util.Formatter");
jQuery.sap.require("com.springer.financefscmapp.util.Controller");

com.springer.financefscmapp.util.Controller.extend("com.springer.financefscmapp.view.SecondLevel.BPInvoiceDetail", {

	sEntityPath: null,
	parameter: 0,
	countCalls: 0,
	mailaddress: "",
	i18model: {},
	//directURL: false,

	/**
	 * Called when the detail list controller is instantiated.
	 */
	onInit: function() {
		this.getView().addEventDelegate({
			onAfterShow: jQuery.proxy(function(evt) {
				this.onAfterShow(evt);
			}, this)
		});
		this.getRouter().attachRoutePatternMatched(this.onRouteMatched, this);
	},

	onRouteMatched: function(oEvent) {
		var oParameters = oEvent.getParameters();
		// when detail navigation occurs, update the binding context
		if (oParameters.name !== "_BPInvoiceDetail") {
			return;
		}
		this.i18model = this.getView().getModel("i18n").getResourceBundle();
		
		var UserPreferences = sap.ui.getCore().getModel("UserPreferences");
		var bReplace = sap.ui.Device.system.phone ? false : true;
		if(typeof UserPreferences === "undefined") {
			console.log("Redirect to start");
			this.getRouter().navTo("_A1_firstVisitCheck", {
				currentView: this.getView()
			}, bReplace);
			return;
		}
		
		this.sEntityPath = "/" + oParameters.arguments.entity;
		var oView = this.getView();
		oView.bindElement(this.sEntityPath);
		
		//Check if the data is already on the client
		if (!oView.getModel().getData(this.sEntityPath)) {
			// Check that the entity specified actually was found.
			oView.getElementBinding().attachEventOnce("dataReceived", jQuery.proxy(function() {
				var oData = oView.getModel().getData(this.sEntityPath);
				if (!oData) {
					this.showEmptyView();
				}
			}, this));
		}
		
		// activate first tab
		var idIconTabBar = this.getView().byId("idIconTabBar");
		if (idIconTabBar.getSelectedKey() !== "Details" ) {
			idIconTabBar.setSelectedKey("Details");
		}
	},

	onAfterShow: function() {
		console.log("Entered: BPInvoiceDetail");
		var UserPreferences = sap.ui.getCore().getModel("UserPreferences");

		var oView = this.getView();
		var openItemINV = oView.byId("openItemINV");
		var contextModel = sap.ui.getCore().getModel("ContextModel");
		if (contextModel.isOpen === "O") {
			openItemINV.removeStyleClass("classGreenicon");
			openItemINV.removeStyleClass("classYellowicon");
			openItemINV.addStyleClass("classRedIcon");
		} else if (contextModel.isOpen === "X") {
			openItemINV.removeStyleClass("classGreenicon");
			openItemINV.removeStyleClass("classRedIcon");
			openItemINV.addStyleClass("classYellowicon");
		} else {
			openItemINV.removeStyleClass("classRedIcon");
			openItemINV.removeStyleClass("classYellowicon");
			openItemINV.addStyleClass("classGreenicon");
		}

		// only show details for Dispute case, when they are available
		if (contextModel.disputeCase === "") {
			oView.byId("DisV1").setVisible(false);
			oView.byId("DisV2").setVisible(false);
			oView.byId("DisV3").setVisible(false);
			oView.byId("DisV4").setVisible(false);
		} else {
			oView.byId("DisV1").setVisible(true);
			oView.byId("DisV2").setVisible(true);
			oView.byId("DisV3").setVisible(true);
			oView.byId("DisV4").setVisible(true);
		}

		// only show details for Promise 2 pay, when they are available
		if (contextModel.FinPromisedAmt > 0) {
			oView.byId("PspV1").setVisible(true);
			oView.byId("PspV2").setVisible(true);
			oView.byId("PspV3").setVisible(true);
			oView.byId("PspV4").setVisible(true);
			oView.byId("PspV5").setVisible(true);
			oView.byId("PspV6").setVisible(true);
			oView.byId("PspV7").setVisible(true);
			oView.byId("PspV8").setVisible(true);
			oView.byId("PspV9").setVisible(true);
			oView.byId("PspV10").setVisible(true);
		} else {
			oView.byId("PspV1").setVisible(false);
			oView.byId("PspV2").setVisible(false);
			oView.byId("PspV3").setVisible(false);
			oView.byId("PspV4").setVisible(false);
			oView.byId("PspV5").setVisible(false);
			oView.byId("PspV6").setVisible(false);
			oView.byId("PspV7").setVisible(false);
			oView.byId("PspV8").setVisible(false);
			oView.byId("PspV9").setVisible(false);
			oView.byId("PspV10").setVisible(false);
		}
		if(UserPreferences.onlineStatus) {
			oView.byId("sendMailBID").setVisible(true);
		} else {
			oView.byId("sendMailBID").setVisible(false);
		}
		var oNavTypeModel = new sap.ui.model.json.JSONModel();  
		if(UserPreferences.onlineStatus && !UserPreferences.deviceAppContext) {
			oNavTypeModel.setData({  
			  itemType : "Navigation"  
			});  
		} else {
			oNavTypeModel.setData({  
			  itemType : "Inactive"   
			});  
		}
		oView.setModel(oNavTypeModel, "UIModel");  
		
		if (contextModel.invoice !== "") {
			oView.byId("invoiceDetail").setTitle("Invoice Detail " + contextModel.invoice);
		}
		oView.setBusy(false);

	},

	onDetailSelect: function(oEvent) {

		//var idIconTabBar = this.getView().byId("idIconTabBar");

		var key = oEvent.getParameters().key;
		switch (key) {
			case "Details":
				//sap.m.MessageToast.show("Info");
				break;
			case "Attachments":
				var sAggregationPath = "";
				var UserPreferences = sap.ui.getCore().getModel("UserPreferences");
				if (UserPreferences.AppView === "OpenItemLive") {
					sAggregationPath = this.sEntityPath + "/ATTACHMENT_INFO_LISTSet";
				} else {
					sAggregationPath = this.sEntityPath + "/ATTACHMENT_INFO_LIST_SVDSet";
				}
				var oTable = this.getView().byId("attachmentList");
				oTable.bindAggregation("items", sAggregationPath, sap.ui.xmlfragment(
					"com.springer.financefscmapp.view.HelpDialogs.DetailInvoiceAttach", this));
				//sap.m.MessageToast.show("Attach"); 
				break;
			case "Actions":
				console.log("BP Detail Actions");
				sap.m.MessageToast.show("Not Yet Implemented");
				break;
			case "Notes":
				console.log("BP Detail Notes");
				sap.m.MessageToast.show("Not Yet Implemented");
				break;
			default:
		}
	},

	showEmptyView: function() {
		this.getRouter().myNavToWithoutHash({
			currentView: this.getView(),
			targetViewName: "com.springer.financefscmapp.view.HelpDialogs.NotFound",
			targetViewType: "XML"
		});
	},
	
	getGuid: function() {
	  function s4() {
	    return Math.floor((1 + Math.random()) * 0x10000)
	      .toString(16)
	      .substring(1);
	  }
	  return s4() + s4() + "-" + s4() + "-" + s4() + "-" + s4() + "-" + s4() + s4() + s4();
	},

	handleSelectionChange: function(evt) {
		this.getView().setBusy(true);
		var oModel = this.getView().getModel();

		var oItem    = evt.getSource().getBindingContext();
		var archivId = oItem.getProperty("ArchivId");
		var reserve  = oItem.getProperty("Reserve");
		//var arcDocId = encodeURIComponent(oItem.getProperty("ArcDocId"));
		var arcDocIdClean = oItem.getProperty("ArcDocId");
		var uniqueID = this.getGuid( );
		console.log(uniqueID);
		
		var UserPreferences = sap.ui.getCore().getModel("UserPreferences");
		var stringDate = new Date().getTime().toString().substring(0,13);
		var that = this;
		var oEntry = {
			"UserId": UserPreferences.UserId,
			"OneTimeId": uniqueID,
			"StringTS": stringDate,
			"Timestamp": null,
			"ArchivId": archivId,
			"ArcDocId": arcDocIdClean,
			"Reserve": reserve,
			"Used": 0
		};
		oModel.create("/ATTACHMENT_ONE_TIME_IDSet", oEntry, null, 
			function() {
				console.log("File Download Register Okay");
				var urlPdfDirect =  "/sap/opu/odata/sbmc/MOBILE_FIFS_SRV/ATTACHMENT_BINARYSet(UserId='"    + UserPreferences.UserId 
		                                                                                 + "',OneTimeId='" + uniqueID
		                                                                                 + "',StringTS='"  + stringDate + "')/$value";
				sap.m.MessageToast.show("Document Download Started");
				that.getView().setBusy(false);
				window.open(urlPdfDirect, "_blank", "location=yes,toolbar=yes,toolbarposition=bottom");
			}, function() {
				that.getView().setBusy(false);
				sap.m.MessageToast.show("File Download Register Failed");
				console.log("File Download Register Failed");
			}
		);
	},

	sendInvoicesPerMail: function() {
    	var input;
		this.getView().setBusy(true);
		this.mailaddress = "";
		var contextModel = sap.ui.getCore().getModel("ContextModel"); 
		var that = this;
		var oModel = this.getView().getModel();
		oModel.read("/GET_EMAIL_FOR_PARTNERSet('" + contextModel.partner + "')", null, null, true,
			function(oData, oResponse) {
				input = prompt("E-Mail Receiver?", oData.EvMail);
				if (input === null) {
					that.getView().setBusy(false);
					return;
				} else {
					that.mailaddress = input;
					that.prepareInvoiceMail(that.mailaddress);
				}
			}, function() {
				input = prompt("E-Mail Receiver?", this.mailaddress);
				if (input === null) {
					that.getView().setBusy(false);
					return;
				} else {
					that.mailaddress = input;
					that.prepareInvoiceMail(that.mailaddress);
				}
			}
		);
	},
	
	prepareInvoiceMail: function(email) {
		var oItem    = {};
		var archivId = "";
		var arcDocIdClean = "";
		var arcDocId = "";
		var reserve  = "";
		var listInvoices = this.getView().byId("attachmentList").getSelectedItems();
		this.countCalls = listInvoices.length;
		
		var minOneLoop = false;
		for (var i = 0; i < listInvoices.length; i++) {
			oItem = listInvoices[i].getBindingContext();
			archivId = oItem.getProperty("ArchivId");
			arcDocIdClean = oItem.getProperty("ArcDocId");
			arcDocId = encodeURIComponent(oItem.getProperty("ArcDocId"));
			reserve = oItem.getProperty("Reserve");
			this.sendInvoiceMail(archivId, arcDocId, arcDocIdClean, reserve, email);
			minOneLoop = true;
		}
		if (minOneLoop === false) {
			this.getView().setBusy(false);
		}
        alert(this.i18model.getText("sendEmailAlert"));
	},
		
	sendInvoiceMail: function(archivId, arcDocId, arcDocIdClean, reserve, email) {
		var that = this;
		var oEntry = {
				"ArchivId": archivId,
				"ArcDocId": arcDocIdClean,
				"Reserve": reserve,
				"Mailadress": email
			};
		
		var oModel = this.getView().getModel();
		if( typeof oModel !== "undefined" ) {
			oModel.create("/ATTACHMENT_INV_MAILSet", oEntry, null,
				function() {
					that.onUpdateFinished();
				},
				function(oError) {
					that.getView().setBusy(false);
					sap.m.MessageToast.show("communication error");
				}	
			);
		} else {
			sap.m.MessageToast.show("Model error");
		}
	},

	onUpdateFinished: function() {
// check if all updates are ready and start the refresh 1 second later asynchronous
		this.parameter++;
		console.log("loop" + this.parameter + ":" + this.countCalls);
		if (this.parameter >= this.countCalls) {
            this.parameter = 0;
            var that = this;
            window.setTimeout(function(){ 
                that.getView().getModel().refresh(true);
                sap.m.MessageToast.show(that.i18model.getText("sendSuccessfully"));
            	that.getView().setBusy(false);
            }, 1000);
		}
	},
	
	navToPartner: function() {
        this.getView().setBusy(true);
		var contextModel = sap.ui.getCore().getModel("ContextModel");
		var UserPreferences = sap.ui.getCore().getModel("UserPreferences");
		var entityString = "";
		if (UserPreferences.AppView === "OpenItemLive") {
			entityString = "OPEN_ITEM_BP_OVERVIEWSet('" + contextModel.partner + "')";
		} else {
			entityString = "OPEN_ITEMS_SAVED_PER_USERSet(UserId='" + UserPreferences.UserId + "',Partner='" + contextModel.partner + "')";
		}
		contextModel.navFromPartnerToMD = entityString;
		sap.ui.getCore().setModel(contextModel, "ContextModel");
		var bReplace = sap.ui.Device.system.phone ? false : true;
		this.getRouter().navTo("_BPOverviewMD_Master", {
			currentView: this.getView()
		}, bReplace);
	},
	
	
	onNavBack: function() {
		//this.getView().setBusy(true);
		
		var UserPreferences = sap.ui.getCore().getModel("UserPreferences");
		var contextModel = sap.ui.getCore().getModel("ContextModel"); 
		if (typeof contextModel === "undefined") {
			contextModel = new sap.ui.model.json.JSONModel();
		}
		if (typeof contextModel.backentity === "undefined") {
			if (UserPreferences.AppView === "OpenItemLive") {
				contextModel.backentity = "OPEN_ITEM_BP_OVERVIEWSet('" + contextModel.partner + "')";
			} else {
				contextModel.backentity = "OPEN_ITEMS_SAVED_PER_USERSet(UserId='" + UserPreferences.UserId + "',Partner='" + contextModel.partner + "')";
			}
			console.log("Entity back navigation set afterwords:" + contextModel.backentity);
		}
		contextModel.showInvoiceScreen  = true;
		sap.ui.getCore().setModel(contextModel, "ContextModel");
		

		var bReplace = sap.ui.Device.system.phone ? false : true;
		if(UserPreferences.PrefereListMode === "X") { // || sap.ui.Device.system.phone === true) {
			this.getRouter().navTo("_BPInvoiceList", {
				currentView: this.getView(),
				entity: contextModel.backentity
			}, bReplace);
		} else {
			contextModel.showInvoiceScreen  = true;
			sap.ui.getCore().setModel(contextModel, "ContextModel");
			this.navToPartner( );
		}
		//window.history.go(-1);
	},
	handleNavToHome: function() {
		var bReplace = sap.ui.Device.system.phone ? false : true;
		this.getRouter().navTo("_A2_Welcome", {
			currentView: this.getView()
		}, bReplace);
	}
});