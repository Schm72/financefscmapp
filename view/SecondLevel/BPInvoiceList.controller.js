jQuery.sap.require("com.springer.financefscmapp.util.Formatter");
jQuery.sap.require("com.springer.financefscmapp.util.Controller");

com.springer.financefscmapp.util.Controller.extend("com.springer.financefscmapp.view.SecondLevel.BPInvoiceList", {

	UserPreferences: {},
	currentEntity: null,
	i18model: {},

	onInit: function() {
		this.UserPreferences = sap.ui.getCore().getModel("UserPreferences");
		if (!this.UserPreferences) {
			console.log("Redirect to start");
			this.getRouter().navTo("_A1_firstVisitCheck", {
				currentView: this.getView()
			}, this.bReplace);
			return;
		}
		this.getView().addEventDelegate({
			onAfterShow: jQuery.proxy(function(evt) {
				this.onAfterShow(evt);
			}, this)
		});
		this.getRouter().attachRoutePatternMatched(this.onRouteMatched, this);
	},

	onRouteMatched: function(evt) {
		var oParameters = evt.getParameters();
		// check that the called view name is like expected
		if (oParameters.name !== "_BPInvoiceList") {
			return;
		}
		this.i18model = this.getView().getModel("i18n").getResourceBundle();
		
		// get current userPreference object
		this.UserPreferences = sap.ui.getCore().getModel("UserPreferences");
		
		if(typeof this.UserPreferences === "undefined") {
			console.log("Redirect to start");
			this.getRouter().navTo("_A1_firstVisitCheck", {
				currentView: this.getView()
			}, false);
			return;
		}
		
		this.currentEntity = oParameters.arguments.entity;
		var sAggregationPath = "";
		if (this.UserPreferences.AppView === "OpenItemLive") {
			sAggregationPath = "/" + this.currentEntity + "/OPEN_ITEM_BP_DETAILVIEWSet";
		} else {
			sAggregationPath = "/" + this.currentEntity + "/OPEN_ITEMS_SAVED_INVOICES_PER_USERSet";
		}
		
		var contextModel = {};
		contextModel = sap.ui.getCore().getModel("ContextModel");
		/*
// check if we need to rebind the aggregation (when live / fav mode changed OR when list View / master detail view change)
		if( typeof this.UserPreferences.oldListEnteredOIListView === "undefined" || this.UserPreferences.oldListEnteredOIListView !== this.UserPreferences.enteredOIListView || 
			typeof this.UserPreferences.oldInvoiceAggregationPath === "undefined" || this.UserPreferences.oldInvoiceAggregationPath !== sAggregationPath ||
			typeof contextModel.partnerOld  === "undefined" || contextModel.partnerOld  !==  contextModel.partner ||
			typeof this.UserPreferences.aFiltersInv !== "undefined" ) {
console.log("===BIND CHANGE===");
*/
			this.UserPreferences.oldInvoiceAggregationPath = sAggregationPath;
			sap.ui.getCore().setModel(this.UserPreferences, "UserPreferences");
			contextModel.partnerOld = contextModel.partner;
			sap.ui.getCore().setModel(contextModel, "ContextModel");

			var aFilters = [];
			var aSorters = [];
			
			/*
			if( this.UserPreferences.aFiltersInv && this.UserPreferences.aFiltersInv.length > 0  ) {
				aFilters = this.UserPreferences.aFiltersInv;
			}
			if ( this.UserPreferences.aSortersInv && this.UserPreferences.aSortersInv.length > 0  ) {
				aSorters = this.UserPreferences.aSortersInv;
			}
			*/
			if (!this.UserPreferences.onlineStatus) {
				aSorters.push(new sap.ui.model.Sorter("DocumentDate", true));
			}
			var oTable = this.getView().byId("idInvListTable");
			oTable.unbindAggregation("items");
			oTable.bindAggregation("items", {
								path     : sAggregationPath, 
								template : sap.ui.xmlfragment("com.springer.financefscmapp.view.HelpDialogs.RowFiDocument", this),
								sorter   : aSorters,
								filters  : aFilters
							}
						);
/*
		} else {
			this.getView().setBusy(false); 
			console.log("No Binding change");
		}
*/

		//Openitem
		this.mGroupFunctions = {
			Openitem: function(oContext) {
				var name = oContext.getProperty("Openitem");
				return {
					key: name,
					text: name
				};
			},
			AddInfos: function(oContext) {
				var name = oContext.getProperty("AddInfos");
				return {
					key: name,
					text: name
				};
			}
		};

	},

	onAfterShow: function() {
		console.log("Entered: BPInvoiceList");
		var oView = this.getView();
		var contextModel = sap.ui.getCore().getModel("ContextModel");
		var titlePage;
		if (sap.ui.Device.system.phone === true) {
			titlePage = "Invoices BP";
		} else {
			titlePage = "Invoices Partner";
		}
		if (contextModel.Partner !== "") {
			titlePage = titlePage + " " + contextModel.partner;
		}
		if (this.UserPreferences.AppView === "OpenItemLive") {
			oView.byId("invoiceOverviewBP").setTitle(titlePage);
		} else {
			if (sap.ui.Device.system.phone === true) {
				oView.byId("invoiceOverviewBP").setTitle("Fav " + titlePage);
			} else {
				oView.byId("invoiceOverviewBP").setTitle("Favorite " + titlePage);
			}
		}
		oView.setBusy(false);
		// update filter bar
		oView.byId("vsdFilterBar").setVisible(false);
		oView.byId("vsdFilterLabel").setText("");

	},

	onInvoiceSearch: function() {
		var oView = this.getView();
		var UserPreferences = sap.ui.getCore().getModel("UserPreferences");
		var aSorters = [];
		/*
		if ( UserPreferences.aSortersInv && UserPreferences.aSortersInv.length > 0  ) {
			aSorters = UserPreferences.aSortersInv;
		}
		*/
		// add filter for search
		var filters = [];
		var searchString = oView.byId("InvoiceSearchText").getValue();
		if (searchString && searchString.length > 0) {
			var filter1 = new sap.ui.model.Filter("SapDocumentId", sap.ui.model.FilterOperator.EQ, searchString);
			//var filter2 = new sap.ui.model.Filter("PartnerDesc", sap.ui.model.FilterOperator.Contains, searchString);
			filters = [filter1];
		} else {
			filters = [];
		}

		// update list binding
		console.log("Update List Binding");
		var oTable   = oView.byId("idInvListTable");
		oTable.unbindAggregation("items");
		oTable.bindAggregation("items", {
							path     : UserPreferences.oldInvoiceAggregationPath, 
							template : sap.ui.xmlfragment("com.springer.financefscmapp.view.HelpDialogs.RowFiDocument", this),
							sorter   : aSorters,
							filters  : filters
						}
					);
		if (searchString && searchString.length > 0) {
			oView.byId("vsdFilterBar").setVisible(true);
			oView.byId("vsdFilterLabel").setText(searchString);
		} else {
			oView.byId("vsdFilterBar").setVisible(false);
			oView.byId("vsdFilterLabel").setText(searchString);
		}
	},

	handleViewSettingsDialogButtonPressed: function(oEvent) {
		if (!this._oDialog) {
			this._oDialog = sap.ui.xmlfragment("com.springer.financefscmapp.view.HelpDialogs.Dialog_InvItems", this);
		}
		// toggle compact style
		jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialog);
		this._oDialog.open();
	},
	handleConfirmInvList: function(oEvent) {
		var oView    = this.getView();
		var mParams  = oEvent.getParameters();

		// apply sorter to binding
		// (grouping comes before sorting)
		var sPath;
		var bDescending;
		var aSorters = [];
		var aFilters = [];

		/*  GROUP  */
		if (mParams.groupItem) {
			console.log("Group Items");
			sPath = mParams.groupItem.getKey();
			bDescending = mParams.groupDescending;
			var vGroup = this.mGroupFunctions[sPath];
			aSorters.push(new sap.ui.model.Sorter(sPath, bDescending, vGroup));
		}

		/*  SORT  */
		if (mParams.sortItem) {
			console.log("Sort Items");
			sPath = mParams.sortItem.getKey();
			bDescending = mParams.sortDescending;
			aSorters = [];
			aSorters.push(new sap.ui.model.Sorter(sPath, bDescending));
		}

		/*  FILTER  */
		if (mParams.filterItems) {
			console.log("Filter Items");
			// apply filters to binding
			jQuery.each(mParams.filterItems, function(i, oItem) {
				var aSplit = oItem.getKey().split("___");
				var sColumn = aSplit[0];
				var sOperator = aSplit[1];
				var sValue = aSplit[2];
				var sSelectionOperator;

				switch (sOperator) {
					case "EQ":
						sSelectionOperator = sap.ui.model.FilterOperator.EQ;
						break;
					case "NE":
						sSelectionOperator = sap.ui.model.FilterOperator.NE;
						break;
					case "CS":
						sSelectionOperator = sap.ui.model.FilterOperator.Contains;
						break;
					case "GE":
						sSelectionOperator = sap.ui.model.FilterOperator.GE;
						break;
					case "GT":
						sSelectionOperator = sap.ui.model.FilterOperator.GT;
						break;
					case "LE":
						sSelectionOperator = sap.ui.model.FilterOperator.LE;
						break;
					case "LT":
						sSelectionOperator = sap.ui.model.FilterOperator.LT;
						break;
						
					//case "BT":
					//   sap.ui.model.FilterOperator.BT
					//   break;
				}
				var oFilter;
				oFilter = new sap.ui.model.Filter(sColumn, sSelectionOperator, sValue);
				aFilters.push(oFilter);
			});
		}
		
		this.UserPreferences = sap.ui.getCore().getModel("UserPreferences");
		var oTable   = this.getView().byId("idInvListTable");
		oTable.unbindAggregation("items");
		oTable.bindAggregation("items", {
							path     : this.UserPreferences.oldInvoiceAggregationPath, 
							template : sap.ui.xmlfragment("com.springer.financefscmapp.view.HelpDialogs.RowFiDocument", this),
							sorter   : aSorters,
							filters  : aFilters
						}
					);

		/*
		// save filter and sorter 
		this.UserPreferences.aSortersInv = aSorters;
		this.UserPreferences.aFilterString = mParams.filterString;
		this.UserPreferences.aFiltersInv = aFilters;
		sap.ui.getCore().setModel(this.UserPreferences, "UserPreferences");
		*/
		
		// update filter bar
		oView.byId("vsdFilterBar").setVisible(aFilters.length > 0);
		oView.byId("vsdFilterLabel").setText(mParams.filterString);
	},

	// Take care of the navigation through the hierarchy when the user selects a table row
	handleSelectionChange: function(oEvent, that, partner) {
		var oItem = oEvent.getSource();

		if (typeof that === "undefined") {
			that = this;
		}
		var contextModel = sap.ui.getCore().getModel("ContextModel");
		if (typeof contextModel === "undefined") {
			contextModel = new sap.ui.model.json.JSONModel();
		}
		if (typeof this.currentEntity === "undefined" || this.currentEntity === null) {
			if (typeof partner !== "undefined") {
				contextModel.partner = partner;
			}
			if (typeof contextModel.partner !== "undefined" && contextModel.partner !== "") {
				if (Object.keys(this.UserPreferences).length < 1) {
					this.UserPreferences = sap.ui.getCore().getModel("UserPreferences");
				}
				if (this.UserPreferences.AppView === "OpenItemLive") {
					this.currentEntity = "OPEN_ITEM_BP_OVERVIEWSet('" + contextModel.partner + "')";
				} else {
					this.currentEntity = "OPEN_ITEMS_SAVED_PER_USERSet(UserId='" + this.UserPreferences.UserId + "',Partner='" + contextModel.partner +
						"')";
				}
			}
		}
		contextModel.backentity = this.currentEntity;
		contextModel.isOpen = oItem.getBindingContext().getProperty("Openitem");
		contextModel.invoice = com.springer.financefscmapp.util.Formatter.overlayTenZero(oItem.getBindingContext().getProperty("SapDocumentId"));
		contextModel.disputeCase = oItem.getBindingContext().getProperty("DisputeCase");
		contextModel.FinPromisedAmt = oItem.getBindingContext().getProperty("FinPromisedAmt");
		sap.ui.getCore().setModel(contextModel, "ContextModel");

		var bReplace = sap.ui.Device.system.phone ? false : true;
		that.getRouter().navTo("_BPInvoiceDetail", {
			from: "_BPInvoiceList",
			entity: oItem.getBindingContext().getPath().substr(1),
			docid: contextModel.invoice
		}, bReplace);
	},
	navToPartner: function() {
		var contextModel = sap.ui.getCore().getModel("ContextModel");
		var UserPreferences = sap.ui.getCore().getModel("UserPreferences");
		var entityString = "";
		if (UserPreferences.AppView === "OpenItemLive") {
			entityString = "OPEN_ITEM_BP_OVERVIEWSet('" + contextModel.partner + "')";
		} else {
			entityString = "OPEN_ITEMS_SAVED_PER_USERSet(UserId='" + UserPreferences.UserId + "',Partner='" + contextModel.partner + "')";
		}
		contextModel.navFromPartnerToMD = entityString;
		sap.ui.getCore().setModel(contextModel, "ContextModel");
		var bReplace = sap.ui.Device.system.phone ? false : true;
		this.getRouter().navTo("_BPOverviewMD_Master", {
			currentView: this.getView()
		}, bReplace);
	},
	onNavBack: function() {
		var bReplace = sap.ui.Device.system.phone ? false : true;
		if (Object.keys(this.UserPreferences).length < 1) {
			this.UserPreferences = sap.ui.getCore().getModel("UserPreferences");
		}
		if (this.UserPreferences.PrefereListMode === "X") {
			this.getView().setBusy(true);
			this.getRouter().navTo("_BPOverviewList", {
				currentView: this.getView()
			}, bReplace);
		} else {
			if (sap.ui.Device.system.phone) {
				this.getView().setBusy(true);
				this.getRouter().navTo("_BPOverviewMD_Master", {
					currentView: this.getView()
				}, bReplace);
			} else {
				var contextModel = sap.ui.getCore().getModel("ContextModel");
				contextModel.showInvoiceScreen = true;
				sap.ui.getCore().setModel(contextModel, "ContextModel");
				this.navToPartner();
			}
		}
		//window.history.go(-1);
	},
	handleNavToHome: function() {
		var bReplace = sap.ui.Device.system.phone ? false : true;
		this.getRouter().navTo("_A2_Welcome", {
			currentView: this.getView()
		}, bReplace);
	}
});