jQuery.sap.require("com.springer.financefscmapp.util.Formatter");
jQuery.sap.require("com.springer.financefscmapp.util.Controller");

com.springer.financefscmapp.util.Controller.extend("com.springer.financefscmapp.view.SecondLevel.MessageCreator", {

	selectedDepartment: "",
	selectedCategory: "",
	feedbacktext: "",
	i18model: {},

	onInit: function() {
		this.getRouter().attachRoutePatternMatched(this.onRouteMatched, this);

		this.getView().addEventDelegate({
			onAfterShow: jQuery.proxy(function(evt) {
				this.onAfterShow(evt);
			}, this)
		});
	},

	onRouteMatched: function(evt) {
		// check that the called view name is like expected
		if (evt.getParameter("name") !== "_MessageCreator") {
			return;
		}
		this.i18model = this.getView().getModel("i18n").getResourceBundle();

		var UserPreferences = sap.ui.getCore().getModel("UserPreferences");
		var bReplace = sap.ui.Device.system.phone ? false : true;
		if (typeof UserPreferences === "undefined") {
			console.log("Redirect to start");
			this.getRouter().navTo("_A1_firstVisitCheck", {
				currentView: this.getView()
			}, bReplace);
			return;
		}
	},
	onAfterShow: function() {
		if (typeof this.oDepartmentCategoryModel === "undefined") {
			this.oDepartmentCategoryModel = sap.ui.getCore().getModel("DepartmentCategoryModel");
		}
		if (typeof this.oDepartmentCategoryModel === "undefined") {
			sap.m.MessageToast.show("Problems while loading message context");
		}
		//this.getView().setModel(oModel);
		var oComboBoxDepartment = this.getView().byId("departmentSelector");
		oComboBoxDepartment.setModel(this.oDepartmentCategoryModel);
		oComboBoxDepartment.bindAggregation("items", "/Messagedetails",
			new sap.ui.core.Item({
				key: "{Department}",
				text: "{DepartmentTXT}"
			}));

		this.feedbacktext  = this.getView().byId("MessageTextContent").getValue();
		this.selectedDepartment = "";
		this.selectedCategory = "";
		this.getView().byId("departmentSelector").setSelectedItem(null);
		this.getView().byId("departmentSelector").focus();
		this.getView().byId("categorySelector").setVisible(false);
		this.getView().byId("MessageTextContent").setVisible(false);
	},

	handleSelectionChangeDEP: function(oEvent) {
		var selectedItem = oEvent.getParameter("selectedItem");
		this.selectedDepartment = selectedItem.getKey();
		console.log(this.selectedDepartment);

		this.getView().byId("MessageTextContent").setVisible(false);
		var oComboBoxCategory = this.getView().byId("categorySelector");
		var vItem = oComboBoxCategory.getSelectedItem();
		oComboBoxCategory.setValue(null);
		oComboBoxCategory.removeItem(vItem);
		oComboBoxCategory.setVisible(true);
		oComboBoxCategory.setModel(this.oDepartmentCategoryModel);
		var vPath = oEvent.getParameter("selectedItem").getBindingContext().getPath() + "/Details";
		oComboBoxCategory.bindAggregation("items", vPath,
			new sap.ui.core.Item({
				key: "{Category}",
				text: "{CategoryTXT}"
			}));

	},
	handleSelectionChangeCAT: function(oEvent) {
		var messageContent = this.getView().byId("MessageTextContent");
		var selectedItem = oEvent.getParameter("selectedItem");
		if (selectedItem === null) {
			messageContent.setVisible(false);
			return;
		}
		this.selectedCategory = selectedItem.getKey();
		messageContent.setVisible(true);
	},

	onNewMessage: function(oEvent) {
		this.feedbacktext = oEvent.getParameter("value");
		if (this.feedbacktext === "") {
			sap.m.MessageToast.show("Message is empty");
			return;
		}
		this.sendMessage(this.feedbacktext,"_MessageCollector");
	},
	sendMessage: function(vMessage,vNavTarget) {
		if (typeof vMessage === "undefined" || vMessage === "") {
			vMessage = this.getView().byId("MessageTextContent").getValue();
		}
		if (this.selectedDepartment === "") {
			sap.m.MessageToast.show("Please enter a Target Department");
			this.getView().byId("departmentSelector").focus();
			return;
		}
		if (this.selectedCategory === "") {
			sap.m.MessageToast.show("Please enter a Category");
			this.getView().byId("categorySelector").focus();
			return;
		}
		var transObj = {};
		transObj.feedbacktext = vMessage;
		transObj.selectedDepartment = this.selectedDepartment;
		transObj.selectedCategory = this.selectedCategory;
		this.getEventBus().publish("MessageCollector", "UserMessage", transObj);
		var bReplace = sap.ui.Device.system.phone ? false : true;
		this.getRouter().navTo(vNavTarget, {currentView: this.getView()}, bReplace);
	},
	askForSaveWhenThereIsText: function(vNavTarget) {
		this.feedbacktext = this.getView().byId("MessageTextContent").getValue();
		var that = this;
		var bReplace = sap.ui.Device.system.phone ? false : true;
		if (this.feedbacktext === "" || this.selectedDepartment === "") {
			that.getRouter().navTo(vNavTarget, {currentView: that.getView()}, bReplace);
			return;
		}
		var oSaveDialog = new sap.m.Dialog({
				title: "Send the Message ?",
				modal: true,
				content: [new sap.m.Text({
					text: "Send the Message to " + this.selectedDepartment + " ?"
				})],
				beginButton: new sap.m.Button({
					text: "No just leave",
					icon: "sap-icon://decline",
					press: function() {
						oSaveDialog.close();
						that.getRouter().navTo(vNavTarget, {currentView: that.getView()}, bReplace);
					}
				}),
				endButton: new sap.m.Button({
					text: "Yes send !",
					icon: "sap-icon://accept",
					press: function() {
						oSaveDialog.close();
						that.sendMessage(that.feedbacktext,vNavTarget);
					}
				})
			});
		oSaveDialog.open();
	},

	handleNavToHome: function() {
		this.askForSaveWhenThereIsText("_A2_Welcome");
	},
	onNavBack: function() {
		this.askForSaveWhenThereIsText("_MessageCollector");
	}

});