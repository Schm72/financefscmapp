jQuery.sap.require("com.springer.financefscmapp.util.Formatter");
jQuery.sap.require("com.springer.financefscmapp.util.Controller");

com.springer.financefscmapp.util.Controller.extend("com.springer.financefscmapp.view.A1_FirstVisitCheck", {

	bReplace: false,
	selectedCountries: "",
	selectedRegions: "",
	selectedRegionsOLD: "",
	UserPreferences: {},
	OnlyOnceFirstCall: false,
	i18model: {},
	activeVersion: "Version 1.7.5",

	onInit: function() {
console.log("Init");
		// method is called via the framework automatically when this view is displayed
		// enable the waiting symbol
		this.getView().setBusy(true);
		// stuff which needs to be called after view is loaded
		this.OnlyOnceFirstCall = true;
		this.getView().addEventDelegate({
			onAfterShow: jQuery.proxy(function(evt) {
				this.onAfterShow(evt);
			}, this)
		});
		
		// save the device type in a separat parameter,
		// this parameter will then be saved in a JSON model which is saved for our application and can be reused in other controllers
		var vDevice;
		switch (true) {
			case sap.ui.Device.system.desktop:
				vDevice = "Desktop";
				break;
			case sap.ui.Device.system.tablet && sap.ui.Device.os.android:
				vDevice = "Android Tablet";
				break;
			case sap.ui.Device.os.android:
				vDevice = "Android";
				break;
			case sap.ui.Device.os.ios && sap.ui.Device.system.tablet:
				vDevice = "iPhone Tablet";
				break;
			case sap.ui.Device.os.ios:
				vDevice = "iPhone";
				break;
			case sap.ui.Device.system.phone:
				vDevice = "Phone";
				break;
			default:
				vDevice = "undefined";
		}

		this.UserPreferences = sap.ui.getCore().getModel("UserPreferences");
		if (typeof this.UserPreferences === "undefined") {
			this.UserPreferences = new sap.ui.model.json.JSONModel();
		}
		
		this.UserPreferences.targetURL = "https://mobile-base.springer-sbm.com:443";

		if (this.UserPreferences.onlineStatus !== true) {
			if (vDevice === "Desktop" || vDevice === "undefined") {
				console.log("Online via " + vDevice);
				this.UserPreferences.onlineStatus = true;
				navigator.onLine = true;
			} else {
				var oGetResponse = jQuery.sap.sjax({  
			    	url: this.UserPreferences.targetURL,
			    	type: "GET"
				}); 
				if (oGetResponse.success){  
					console.log("Success Online - oGetResponse " + this.UserPreferences.targetURL);
					this.UserPreferences.onlineStatus = true;
					navigator.onLine = true;
				} else {
					console.log("Offline after A1 oGetResponse " + this.UserPreferences.targetURL);
				}
			}
		}
		this.UserPreferences.device = vDevice;
console.log("Device: " + this.UserPreferences.device);
		
//* check if we on a device app context
		var externalURL = com.springer.financefscmapp.dev.devapp.externalURL;
		var appContext = null;
		if (com.springer.financefscmapp.dev.devapp.devLogon) {
			appContext = com.springer.financefscmapp.dev.devapp.devLogon.appContext;
		}
		if (window.cordova && appContext && !window.sap_webide_companion && !externalURL) {
			this.UserPreferences.deviceAppContext = true;
		} else {
			this.UserPreferences.deviceAppContext = false;
		}
		
		sap.ui.getCore().setModel(this.UserPreferences, "UserPreferences");
		
		this.getRouter().attachRoutePatternMatched(this.onRouteMatched, this);
	},

	onRouteMatched: function(evt) {
		// check that we are in the URL hash pattern http://domain:port/%PATTERN%
		if (evt.getParameter("name") !== "_A1_firstVisitCheck") {
			return;
		}
		if (Object.keys(this.UserPreferences).length < 1) {
			this.UserPreferences = sap.ui.getCore().getModel("UserPreferences");
		}
		this.i18model = this.getView().getModel("i18n").getResourceBundle();
		
		// check if we are just here to reassign the collection region and countries
		if (this.UserPreferences.newCollArea === true) {
			this.triggerNewCollAreaSelection("Reassignment");
			return;
		} else {
console.log("Normal Start Mode");
		}
	},
	
	triggerNewCollAreaSelection: function(vCustomText) {
		console.log("New Coll Area Mode");
			this.getView().byId("UserCheckPage").setShowNavButton(true);

			// save the current user selected values
			this.selectedRegions = this.UserPreferences.selectedRegions;
			this.selectedCountries = this.UserPreferences.selectedCountries;
			// when we have already values set these values to the multiComboBox
			// values saved as comma separated list - therefore a split gives us an array which can be assigned to the multiComboBox
			if (this.selectedRegions !== "") {
				var oComboBoxRegions = this.getView().byId("regionSelector");
				var selectedRegionsArray = this.selectedRegions.split(",");
				oComboBoxRegions.setSelectedKeys(selectedRegionsArray);
				var oComboBoxCountries = this.getView().byId("countrySelector");
				oComboBoxCountries.setVisible(true);
				// bind the available countries for these regions to the country multiComboBox
				oComboBoxCountries.bindAggregation("items", "/REGION_SELECTIONSet('" + this.selectedRegions + "')/COUNTRY_SELECTIONSet",
					new sap
					.ui.core.Item({
						key:  "{Country}",
						text: "{CountryText}"
					}));
				// set the already selected countries to the country multiComboBox
				var selectedCountriesArray = this.selectedCountries.split(",");
				oComboBoxCountries.setSelectedKeys(selectedCountriesArray);
			}
			this.chooseCollArea(false,vCustomText);
	},
	
	onAfterShow: function() {
		if (this.OnlyOnceFirstCall === false) {
			return;
		}
		this.OnlyOnceFirstCall = false;
		if (Object.keys(this.UserPreferences).length < 1) {
			this.UserPreferences = sap.ui.getCore().getModel("UserPreferences");
		}
		var that = this;
		// getting the eventbus and subscribing methods for events
		var oEventBus = this.getEventBus();
		oEventBus.subscribe("FirstVisitCheck", "OnlineModeExist", this.onlineModeExist, this);
		oEventBus.subscribe("FirstVisitCheck", "OnlineModeNew", this.onlineModeNew, this);
		oEventBus.subscribe("FirstVisitCheck", "OfflineMode", this.offlineMode, this);
		// READ: reading user preferences for this app -> to check if we only and iof the user already exist
		var oModel = this.getView().getModel();
		
		//oModel.read("APP_USER_PREFERENCESSet", null, ["$filter=Application eq 'financefscmapp'"], true,
		oModel.read("/APP_USER_PREFERENCESSet", null, null, true,
			function(oData) {
				if (oData.results[0]) {
					console.log("sapSystem: " + oData.results[0].SapSystem);
					switch (oData.results[0].SapSystem) {
						case "MPS":
							that.UserPreferences.targetURL = "https://mobile-base.springer-sbm.com:444";
							break;
						case "MQS":
							that.UserPreferences.targetURL = "https://mobile-base.springer-sbm.com:447";
							break;
						case "MTS":
							that.UserPreferences.targetURL = "https://mobile-base.springer-sbm.com:446";
							break;
						default:
							that.UserPreferences.targetURL = "https://mobile-base.springer-sbm.com:444";
					}
					
					if (that.UserPreferences.onlineStatus !== true) {
						var oGetResponse = jQuery.sap.sjax({  
					    	url: that.UserPreferences.targetURL + "/sap/opu/odata/sbmc/MOBILE_FIFS_SRV/?$format=json",
					    	type: "GET"
						}); 
						if (oGetResponse.success){  
							that.UserPreferences.onlineStatus = true;
							navigator.onLine = true;
						}
					}
					sap.ui.getCore().setModel(that.UserPreferences, "UserPreferences");
					
					if (that.UserPreferences.onlineStatus || window.sap_webide_FacadePreview) {
						// we are online and received data (user already exist and preferences are clear )
						// -> received data are saved in oData entry 
						// we send this information to all methods which subscribed for this event
console.log("OnlineModeExist");
						oEventBus.publish("FirstVisitCheck", "OnlineModeExist", {
							entry: oData.results[0]
						});
					} else {
						// we are offline
console.log("OfflineMode with data");
						oEventBus.publish("FirstVisitCheck", "OfflineMode", {
							entry: oData.results[0]
						});
					}
				} else {
					if (that.UserPreferences.onlineStatus) {
console.log("Probably new User");
						// we are online, but there are no data for this user -> so this must be a new user
						oEventBus.publish("FirstVisitCheck", "OnlineModeNew");
					} else {
						// no data - no connection
console.log("OfflineMode no data");
						oEventBus.publish("FirstVisitCheck", "OfflineMode");
					}
				}
			},
			function(oError) {
				// we are offline, because no communication to the backend is available
				oEventBus.publish("FirstVisitCheck", "OfflineMode");
			}
		);
	},

	updateStatus: function(msg) {
		// getting a DOM element from the App view and modify it
		//sap.m.MessageToast.show(msg);
		//sap.Logger.debug(msg);
		console.log(msg);
	},
	
	chooseCollArea: function(addText,vCustomText) {
		this.getView().setBusy(false);
		var textArea = this.getView().byId("ContextText");
		textArea.setVisible(true);
		if ( typeof vCustomText === "undefined" || vCustomText === "") {
			vCustomText = this.i18model.getText("StartChooseColl");
		}
		if (addText === true) {
			textArea.setValue(textArea.getValue() + "\n" + vCustomText);
		} else {
			textArea.setValue(vCustomText);
		}
		this.getView().byId("regionSelector").setVisible(true);
		this.getView().byId("finishCollArea").setVisible(true);
	},

	onlineModeNew: function() {
		this.getView().setBusy(false);

		var textArea = this.getView().byId("ContextText");
		textArea.setVisible(true);
		textArea.setValue(this.i18model.getText("StartFirstTimeEnter"));

		this.chooseCollArea(true);

		if (Object.keys(this.UserPreferences).length < 1) {
			this.UserPreferences = sap.ui.getCore().getModel("UserPreferences");
		}
		this.UserPreferences.mode = "OnlineModeNew";
		this.UserPreferences.lastvisit = null;
		this.UserPreferences.firstStart = true;
		this.UserPreferences.onlineStatus = true;
		this.UserPreferences.NewMessage = "X";
		this.UserPreferences.PrefereListMode = "X";
		sap.ui.getCore().setModel(this.UserPreferences, "UserPreferences");

		this.updateStatus("OnlineModeNew");
	},

	onlineModeExist: function(sChannel, sEvent, input) {
		var oModel = this.getView().getModel();
		
		if (Object.keys(this.UserPreferences).length < 1) {
			this.UserPreferences = sap.ui.getCore().getModel("UserPreferences");
		}
		this.UserPreferences.mode = "OnlineModeExist";
		if (input.entry.LastvisitTime) {
			this.UserPreferences.lastvisit = com.springer.financefscmapp.util.Formatter.dateSimple(input.entry.LastvisitTime);
		}
		var that = this;
		// set some values
		input.entry.UserCategory = this.activeVersion;
		if(typeof this.UserPreferences.device !== "undefined") {
			input.entry.Lastuseddevice = this.UserPreferences.device;
		}
		// update also on sap side that we have a new app start
		oModel.update("/APP_USER_PREFERENCESSet('" + input.entry.UserId + "')", input.entry, null,
			function() {
				oModel.read("/APP_USER_PREFERENCESSet('" + input.entry.UserId + "')", null, null, true,
					function(oData) {
						if (oData) {
							that.UserPreferences.firstStart = true;
							that.UserPreferences.onlineStatus = true;
							// input.entry
							that.UserPreferences.UserId = oData.UserId;
							that.UserPreferences.selectedRegions = oData.Regions;
							that.UserPreferences.selectedCountries = oData.Countries;
							that.UserPreferences.CountFscmSel = oData.CountFscmSel;
							that.UserPreferences.CountFscmFav = oData.CountFscmFav;
							that.UserPreferences.CountMessages = oData.CountMessages;
							that.UserPreferences.NewMessage = oData.NewMessage;
							that.UserPreferences.PrefereListMode = oData.PrefereListMode;
							that.UserPreferences.PreferedOiFilter = oData.PreferedOiFilter;
							that.UserPreferences.UserActive = oData.UserActive;
							
							if ( that.UserPreferences.selectedRegions === "" || that.UserPreferences.selectedCountries === "" ) {
								that.UserPreferences.newCollArea = true;
							} else {
								that.UserPreferences.newCollArea = false;
							}
							sap.ui.getCore().setModel(that.UserPreferences, "UserPreferences");
							that.getEventBus().publish("A1_FirstVisitCheck", "updUserConfLoadReady");
							that.updateStatus("OnlineModeExist");
							
							if ( that.UserPreferences.newCollArea === true ) {
								console.log("redirect New Coll Area");
								that.triggerNewCollAreaSelection("Region / Country selection is mandatory");
							} else {
								console.log("forward Welcome");
								that.getRouter().navTo("_A2_Welcome", {
									currentView: that.getView()
								}, false);
							}
						}
					}
				);
			},
			function(oError) {
				console.log("Communication error");
				that.getRouter().navTo("_A2_Welcome", {
					currentView: that.getView()
				}, that.bReplace);
			}
		);
	},

	offlineMode: function(sChannel, sEvent, input) {
		this.getView().setBusy(false);

		if (Object.keys(this.UserPreferences).length < 1) {
			this.UserPreferences = sap.ui.getCore().getModel("UserPreferences");
		}
		this.UserPreferences.mode = "OfflineMode";
		this.UserPreferences.lastvisit = null;
		this.UserPreferences.firstStart = true;
		this.UserPreferences.onlineStatus = false;

		if (Object.keys(input).length > 0) {
			this.UserPreferences.UserId = input.entry.UserId;
			this.UserPreferences.selectedRegions = input.entry.Regions;
			this.UserPreferences.selectedCountries = input.entry.Countries;
			this.UserPreferences.CountFscmSel = input.entry.CountFscmSel;
			this.UserPreferences.CountFscmFav = input.entry.CountFscmFav;
			this.UserPreferences.CountMessages = input.entry.CountMessages;
			this.UserPreferences.PrefereListMode = input.entry.PrefereListMode;
			this.UserPreferences.PreferedOiFilter = input.entry.PreferedOiFilter;
			this.UserPreferences.UserActive = input.entry.UserActive;
		}

		sap.ui.getCore().setModel(this.UserPreferences, "UserPreferences");

		//sap.OData.applyHttpClient();
		this.updateStatus(" - OFFLINE MODE - ");

		this.getRouter().navTo("_A2_Welcome", {
			currentView: this.getView()
		}, this.bReplace);
	},

	handleSelectionChangeRegion: function(oEvent) {
		var changedItem = oEvent.getParameter("changedItem");
		var isSelected = oEvent.getParameter("selected");
		var state = "Selected";
		if (!isSelected) {
			state = "Deselected";
		}
		sap.m.MessageToast.show(state + " '" + changedItem.getText() + "'", {
			width: "auto"
		});
	},

	handleSelectionFinishRegion: function(oEvent) {
		var selectedItems = oEvent.getParameter("selectedItems");
		this.selectedRegions = "";
		for (var i = 0; i < selectedItems.length; i++) {
			this.selectedRegions += selectedItems[i].getKey();
			if (i !== selectedItems.length - 1) {
				this.selectedRegions += ",";
			}
		}
		var oComboBoxCountries = this.getView().byId("countrySelector");
		if(this.selectedRegions === "") {
			alert(this.i18model.getText("chooseAregion"));
			this.getView().byId("regionSelector").setPlaceholder(this.i18model.getText("newPlaceholderRegion"));
			this.getView().byId("regionSelector").focus();
			this.selectedCountries = "";
			oComboBoxCountries.setVisible(false);
			return;
		}
		// check if regions changed - only when yes reset country multiComboBox
		oComboBoxCountries.setVisible(true);
		// check if the old region variable is set - when not set from user preferences
		if (this.selectedRegionsOLD === "") {
			if (Object.keys(this.UserPreferences).length < 1) {
				this.UserPreferences = sap.ui.getCore().getModel("UserPreferences");
			}
			if (this.UserPreferences.selectedRegions !== "") {
				this.selectedRegionsOLD = this.UserPreferences.selectedRegions;
			}
		}
		if (this.selectedRegions !== this.selectedRegionsOLD) {
			this.selectedRegionsOLD = this.selectedRegions;
			oComboBoxCountries.bindAggregation("items", "/REGION_SELECTIONSet('" + this.selectedRegions + "')/COUNTRY_SELECTIONSet", new sap.ui.core
				.Item({
					key: "{Country}",
					text: "{CountryText}"
				}));
			// try to set the already selected countries so they are not lost for the user
			var selectedCountriesArray = this.selectedCountries.split(",");
			oComboBoxCountries.setSelectedKeys(selectedCountriesArray);

		}
	},

	handleSelectionChangeCountry: function(oEvent) {
		var changedItem = oEvent.getParameter("changedItem");
		var isSelected = oEvent.getParameter("selected");
		var state = "Selected";
		if (!isSelected) {
			state = "Deselected";
		}
		sap.m.MessageToast.show(state + " '" + changedItem.getText() + "'", {
			width: "auto"
		});
	},

	handleSelectionFinishCountry: function(oEvent) {
		var selectedItems = oEvent.getParameter("selectedItems");
		this.setCountryString(selectedItems);
	},
	
	setCountryString: function(selectedItems) {
		this.selectedCountries = "";
		for (var i = 0; i < selectedItems.length; i++) {
			this.selectedCountries += selectedItems[i].getKey();
			if (i !== selectedItems.length - 1) {
				this.selectedCountries += ",";
			}
		}
	},
	
	finishCollArea: function(noNavBack) {
		var that = this;
		if (Object.keys(this.UserPreferences).length < 1) {
			this.UserPreferences = sap.ui.getCore().getModel("UserPreferences");
		}
		if(this.selectedRegions === "") {
			alert(this.i18model.getText("chooseAregion"));
			this.getView().byId("regionSelector").setPlaceholder(this.i18model.getText("newPlaceholderRegion"));
			this.getView().byId("regionSelector").focus();
			return;
		}
		if(this.selectedCountries === "") {
			var selectedItems = this.getView().byId("countrySelector").getSelectedItems();
			this.setCountryString(selectedItems);
		}
		if(this.selectedCountries === "") {
			alert(this.i18model.getText("chooseAcountry"));
			this.getView().byId("countrySelector").setPlaceholder(this.i18model.getText("newPlaceholderCountry"));
			this.getView().byId("countrySelector").focus();
			return;
		}
		this.UserPreferences.selectedRegions = this.selectedRegions;
		this.UserPreferences.selectedCountries = this.selectedCountries;
		sap.ui.getCore().setModel(this.UserPreferences, "UserPreferences");

		var oModel = this.getView().getModel();
		if (this.UserPreferences.newCollArea === true) {
			oModel.read("/APP_USER_PREFERENCESSet('"+this.UserPreferences.UserId+"')", null, null, true,
				function(oData) {
					if (oData) {
						oData.Regions = that.selectedRegions;
						oData.Countries = that.selectedCountries;
						oModel.update("/APP_USER_PREFERENCESSet('"+that.UserPreferences.UserId+"')", oData, null, null, null);
					}
				},
				function(oError) {
					console.log("Send failed " + oError);
				}
			);
		} else {
			this.getView().setBusy(true);
			var d = new Date();
			var vDate = new Date(d.getUTCFullYear(), d.getUTCMonth() - 1, d.getUTCDate());
			var oEntry = {
				"UserId": "EXTERN",
				"UserActive": "",
				"UserCategory": this.activeVersion,
				"CreationDate": vDate,
				"LastvisitTime": null,
				"PrefereListMode": "X",
				"AppStartScreen": 1,
				"Regions": this.selectedRegions,
				"Countries": this.selectedCountries,
				"NewMessage": "X",
				"PreferedOiFilter": "X"
			};
			oModel.create("/APP_USER_PREFERENCESSet", oEntry, null, 
				function(oData){  
					that.getView().setBusy(false);
					that.UserPreferences.UserId = oData.UserId;
					that.UserPreferences.UserActive = oData.UserActive;
					that.UserPreferences.CountFscmSel = oData.CountFscmSel;
					that.UserPreferences.CountFscmFav = oData.CountFscmFav;
					that.UserPreferences.CountMessages = oData.CountMessages;
					that.UserPreferences.PreferedOiFilter = oData.PreferedOiFilter;
					sap.ui.getCore().setModel(that.UserPreferences, "UserPreferences");
					console.log("User Data Saved: " + oEntry.Category);
				}, function(oError) {
					that.getView().setBusy(false);
					console.log("Send failed " + oError);
				}
			);
		}

		if (noNavBack !== true) {
			this.getRouter().navTo("_A2_Welcome", {
				currentView: this.getView()
			}, this.bReplace);
		} else {
			this.getRouter().navTo("_Profile", {
				currentView: this.getView()
			}, this.bReplace);
		}
	},

	callStoreError: function(sender, error) {
		this.updateStatus("An error occurred in " + sender + " with msg: " + JSON.stringify(error));
	},

	onNavBack: function() {
		this.finishCollArea(true);
	}

});