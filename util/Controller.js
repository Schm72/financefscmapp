jQuery.sap.declare("com.springer.financefscmapp.util.Controller");

sap.ui.core.mvc.Controller.extend("com.springer.financefscmapp.util.Controller", {
	/**
	 * get the event bus of the applciation component
	 * @returns {Object} the event bus
	 */
	getEventBus: function() {
		return sap.ui.getCore().getEventBus();
	},

	/**
	 * get the UIComponent router
	 * @param{Object} this either a view or controller
	 * @returns {Object} the event bus
	 */
	getRouter: function() {
		return sap.ui.core.UIComponent.getRouterFor(this);
	},
	onAddFavoriteController: function(that) {
		var thot = this;
		if( typeof this.oAddFavDialog === "undefined") {
			this.oAddFavDialog = new sap.m.Dialog({
				title: "Add a Favorite",
				modal: true,
				content: [new sap.m.Label({text: "Enter the Partner Number: "}),
                    	  new sap.m.Input({maxLength: 10,id: "PartnerID",type: "Number"})],
				beginButton: new sap.m.Button({
					text: "Add Favorite",
					icon: "sap-icon://accept",
					press: function() {
						var partner = sap.ui.getCore().byId("PartnerID").getValue();
						console.log("Entered Partner: " + partner);
						if (typeof partner === "undefined" || partner === "") {
								sap.m.MessageToast.show("No partner ID entered");
						} else {
							var oModel = that.getView().getModel();
							thot.addRemoveFavorite(true,oModel,"OPEN_ITEM_BP_OVERVIEWSet('" + partner + "')",partner,that);
						}
						thot.oAddFavDialog.close();
					}
				}),
				endButton: new sap.m.Button({
					text: "Abort",
					icon: "sap-icon://decline",
					press: function() {
						thot.oAddFavDialog.close();
					}
				})
			});
		}
		this.oAddFavDialog.open();
	},
	addRemoveFavorite: function(addedFavorite,oModel,oDataPath,partner,that) {
		oModel.read(oDataPath, null, null, true,
			function(oData) {
				if (oData) {
					if(addedFavorite) {
						oData.AddedFavorite = "X";
					} else {
						oData.AddedFavorite = "";
					}
					oModel.update(oDataPath, oData, null, 
						function() {
							that.getView().setBusy(false);
							if(addedFavorite) {
								sap.m.MessageToast.show(that.i18model.getText("OIAddedFav") + " " +  partner );
							} else {
								sap.m.MessageToast.show(that.i18model.getText("OIRemovedFav") + " " +  partner );
							}
						},
						function(oError) {
							that.getView().setBusy(false); 
							sap.m.MessageToast.show(that.i18model.getText("OIUpdatFavFail") + " " +  oError);
						}
					);
				} else {
					that.getView().setBusy(false); 
					sap.m.MessageToast.show(that.i18model.getText("OIFavNoData"));
				}
			},
			function() {
				that.getView().setBusy(false);
				that.showAlertPopup(that.i18model.getText("PartnerDontExist"));
				//
			}
		);
	},
	showAlertPopup: function(vMessage) {
		var that = this;
		console.log("here");
		if( typeof this.oMsgPopupDialog === "undefined") {
			this.oMsgPopupDialog = new sap.m.Dialog({
				title: vMessage,
				modal: true,
				content: [new sap.m.Text({text: vMessage})],
				endButton: new sap.m.Button({
					text: "Okay got it",
					icon: "sap-icon://sys-back",
					press: function() {
						that.oMsgPopupDialog.close();
					}
				})
			});
		}
		this.oMsgPopupDialog.open();
	}
	
});