jQuery.sap.require("com.springer.financefscmapp.util.Formatter");
jQuery.sap.require("com.springer.financefscmapp.util.Controller");

com.springer.financefscmapp.util.Controller.extend("com.springer.financefscmapp.view.BPOverviewMD_Detail", {

	i18model: {},
	navTwoSteps: false,

	/**
	 * Called when the detail list controller is instantiated.
	 */
	onInit: function() {
		this.getView().addEventDelegate({
			onAfterShow: jQuery.proxy(function(evt) {
				this.onAfterShow(evt);
			}, this)
		});

		this.getEventBus().subscribe("BPOverviewMD_Master", "FirstItemSelected", this.checkDeleteButtonVisible, this);

		this.oInitialLoadFinishedDeferred = jQuery.Deferred();
		if (sap.ui.Device.system.phone) {
			//dont wait for the master on a phone
			this.oInitialLoadFinishedDeferred.resolve();
		} else {
			this.getView().setBusy(true);
			this.getEventBus().subscribe("BPOverviewMD_Master", "InitialLoadFinished", this.onMasterLoaded, this);
		}

		this.getRouter().attachRouteMatched(this.onRouteMatched, this);
	},

	/**
	 * online icon formatter
	 */
	onlineIconVisible: function(bIsOffline, bIsPhone) {
		return bIsPhone && bIsOffline;
	},

	onRouteMatched: function(oEvent) {
		var oParameters = oEvent.getParameters();
		jQuery.when(this.oInitialLoadFinishedDeferred).then(jQuery.proxy(function() {
			this.getView().setBusy(false);
			// when detail navigation occurs, update the binding context
			if (oParameters.name !== "_BPOverviewMD_Detail") {
				return;
			}

			var contextModel = sap.ui.getCore().getModel("ContextModel");
			if (typeof contextModel !== "undefined" && typeof contextModel.navFromPartnerToMD !== "undefined" && contextModel.navFromPartnerToMD !== null) {
				this.navTwoSteps = true;
				contextModel.navFromPartnerToMD = null;
			} else {
				this.navTwoSteps = false;
			}
		
			var sEntityPath = "/" + oParameters.arguments.entity;
			this.bindView(sEntityPath);
			contextModel.viewAlreadyBinded = true;
			
			// activate a tab
			var idIconTabBar = this.getView().byId("idIconTabBar");
			var check = idIconTabBar.getSelectedKey( );
			console.log("IconTab Descission:" + check);
			if( contextModel.showInvoiceScreen === true) {
				if (check !== "Invoices") {
					idIconTabBar.setSelectedKey("Invoices");
				}
				contextModel.showInvoiceScreen = false;
				this.handleInvoiceListSelect( );
			} else {
				if (check !== "Details") {
					idIconTabBar.setSelectedKey("Details");
				}
			}
			sap.ui.getCore().setModel(contextModel, "ContextModel");
			
		}, this));
	},
	
	onMasterLoaded: function(sChannel, sEvent, oData) {
		var contextModel = sap.ui.getCore().getModel("ContextModel");
		if ( contextModel.viewAlreadyBinded === true) {
			return;
		}
		console.log("MasterLoaded");
		if (oData.oListItem) {
			this.bindView(oData.oListItem.getBindingContext().getPath());
			this.getView().setBusy(false);
			this.oInitialLoadFinishedDeferred.resolve();
		}
	},


	checkDeleteButtonVisible: function() {
		var UserPreferences = sap.ui.getCore().getModel("UserPreferences");
		var button = this.getView().byId("deleteButton");
		if (UserPreferences.AppView === "OpenItemLive") {
			button.setVisible(false);
		} else {
			button.setVisible(true);
		}
	},

	onAfterShow: function() {
		console.log("Entered: BPOverviewMD_Detail");
		this.i18model = this.getView().getModel("i18n").getResourceBundle();
		
	},

	bindView: function(sEntityPath) {
		var oView = this.getView();
		oView.bindElement(sEntityPath);

		//Check if the data is already on the client
		if (!oView.getModel().getData(sEntityPath)) {
			// Check that the entity specified actually was found.
			oView.getElementBinding().attachEventOnce("dataReceived", jQuery.proxy(function() {
				var oData = oView.getModel().getData(sEntityPath);
				if (!oData) {
					this.showEmptyView();
					this.fireDetailNotFound();
				} else {
					this.fireDetailChanged(sEntityPath);
				}
			}, this));
		} else {
			this.fireDetailChanged(sEntityPath);
		}
		this.checkDeleteButtonVisible();
	},

	showEmptyView: function() {
		this.getRouter().myNavToWithoutHash({
			currentView: this.getView(),
			targetViewName: "com.springer.financefscmapp.view.HelpDialogs.NotFound",
			targetViewType: "XML"
		});
	},

	fireDetailChanged: function(sEntityPath) {
		this.getEventBus().publish("BPOverviewMD_Detail", "Changed", {
			sEntityPath: sEntityPath
		});
	},

	fireDetailNotFound: function() {
		this.getEventBus().publish("BPOverviewMD_Detail", "NotFound");
	},

	onAddFav: function(oEvent) {

		this.getView().setBusy(true);
		this.addedFavorite = false;
		if (oEvent.getParameter("state") === true) {
			this.addedFavorite = true;
		}

		this.currentFavoritePartner = com.springer.financefscmapp.util.Formatter.overlayTenZero(oEvent.getSource().getBindingContext().getProperty(
			"Partner"));

		var oModel = this.getView().getModel();
		var UserPreferences = sap.ui.getCore().getModel("UserPreferences");
		// change the favorite information
		if (UserPreferences.AppView === "OpenItemLive"  || this.addedFavorite === true) {
			this.addRemoveFavorite(this.addedFavorite, oModel, "OPEN_ITEM_BP_OVERVIEWSet('" + this.currentFavoritePartner + "')", this.currentFavoritePartner,
				this);
		} else {
			this.openDeleteConfirmDialog();
		}
	},

	openDialog: function(sI18nKeyword) {
		if (!this._dialog) {
			var id = this.getView().getId();
			var frgId = id + "-msgDialog";
			this._dialog = sap.ui.xmlfragment(frgId, "com.springer.financefscmapp.view.HelpDialogs.MsgDialog", this);
			this.getView().addDependent(this._dialog);
			this._dialogText = sap.ui.core.Fragment.byId(frgId, "dialogText");
		}
		this._dialogText.bindProperty("text", sI18nKeyword);
		this._dialog.open();
	},
	closeDialog: function() {
		this.getView().setBusy(false);
		if (this._dialog) {
			this._dialog.close();
		}
	},
	openDeleteConfirmDialog: function() {
		console.log("deleteDialogStart");
		if (!this._deleteConfirmDialog) {
			var id = this.getView().getId();
			var frgId = id + "-_dialog_DeleteConfirm";
			this._deleteConfirmDialog = sap.ui.xmlfragment(frgId, "com.springer.financefscmapp.view.HelpDialogs.Dialog_DeleteConfirm", this);
			this.getView().addDependent(this._deleteConfirmDialog);
		}
		this._deleteConfirmDialog.open();
	},

	confirmDelete: function() {
		if (this._deleteConfirmDialog) {
			this._deleteConfirmDialog.close();
		}
		if (this.currentFavoritePartner !== "") {
			// delete from favorite table
			var UserPreferences = sap.ui.getCore().getModel("UserPreferences");
			var delPath = "/OPEN_ITEMS_SAVED_PER_USERSet(UserId='" + UserPreferences.UserId + "',Partner='" + this.currentFavoritePartner +
				"')";
			var model = this.getView().getModel();
			if (model) {
				this.getView().setBusy(true);
				model.remove(delPath, {
					success: jQuery.proxy(function() {
						this.getView().setBusy(false);
						UserPreferences.CountFscmFav--;
						sap.ui.getCore().setModel(UserPreferences, "UserPreferences");
						this.onNavBack();
						//this.openDialog("i18n>deleteSuccess");
					}, this),
					error: jQuery.proxy(function() {
						this.getView().setBusy(false);
						this.openDialog("i18n>deleteFailed");
						sap.m.MessageToast.show(this.i18model.getText("OIRemovedFav") + " " + this.currentFavoritePartner );
					}, this)
				});
			}
		}
	},

	closeDeleteConfirmDialog: function() {

		var oSwitch = this.getView().byId("oSwitch");
		if (this.addedFavorite === "X") {
			oSwitch.setState(false);
		} else {
			oSwitch.setState(true);
		}

		this.getView().setBusy(false);
		if (this._deleteConfirmDialog) {
			this._deleteConfirmDialog.close();
		}
	},

	deleteItem: function(oEvent) {
		//this.openDeleteConfirmDialog();
		//var oModel = this.getView().getModel();
		//var UserPreferences = sap.ui.getCore().getModel("UserPreferences");
		this.currentFavoritePartner = com.springer.financefscmapp.util.Formatter.overlayTenZero(oEvent.getSource().getBindingContext().getProperty("Partner"));
		//var oDataPath = "OPEN_ITEMS_SAVED_PER_USERSet(UserId='" + UserPreferences.UserId + "',Partner='" + this.currentFavoritePartner + "')";
		//this.addRemoveFavorite("", oModel, oDataPath, this.currentFavoritePartner, this);
		this.openDeleteConfirmDialog();
	},

	onDetailSelect: function(oEvent) {
		var key = oEvent.getParameters().key;
		switch (key) {
			case "Details":
				//sap.m.MessageToast.show("Info");
				break;
			case "Invoices":
				console.log("BP Detail Invoice List");
				this.handleInvoiceListSelect( );
				break;
			case "Actions":
				console.log("BP Detail Actions");
				sap.m.MessageToast.show("Not Yet Implemented");
				break;
			case "Notes":
				console.log("BP Detail Notes");
				sap.m.MessageToast.show("Not Yet Implemented"); 
				break;
			default:
		}
	},
	handleInvoiceListSelect: function( ) {
		var UserPreferences = sap.ui.getCore().getModel("UserPreferences");
		var oView  = this.getView();
		var currentEntity = oView.getBindingContext().getPath();
		var sAggregationPath = "";
		if (UserPreferences.AppView === "OpenItemLive") {
			sAggregationPath = currentEntity + "/OPEN_ITEM_BP_DETAILVIEWSet";
		} else {
			sAggregationPath = currentEntity + "/OPEN_ITEMS_SAVED_INVOICES_PER_USERSet";
		}
		UserPreferences.oldInvoiceAggregationPath = sAggregationPath;
		sap.ui.getCore().setModel(UserPreferences, "UserPreferences");
		var aFilters = [];
		var aSorters = [];
		/*
		if( UserPreferences.aFiltersInv && UserPreferences.aFiltersInv.length > 0  ) {
			aFilters = UserPreferences.aFiltersInv;
		}
		if ( UserPreferences.aSortersInv && UserPreferences.aSortersInv.length > 0  ) {
			aSorters = UserPreferences.aSortersInv;
		}
		*/
		if (!UserPreferences.onlineStatus) {
			aSorters.push(new sap.ui.model.Sorter("DocumentDate", true));
		}
		var oTable = oView.byId("idInvListTable");
		oTable.bindAggregation("items", {
								    path     : sAggregationPath, 
								    template : sap.ui.xmlfragment("com.springer.financefscmapp.view.HelpDialogs.RowFiDocument", this),
									sorter   : aSorters,
									filters  : aFilters
								}
							);
		// update filter bar
		if( typeof UserPreferences.aFilterString !== "undefined" ) {
			oView.byId("vsdFilterBar").setVisible(UserPreferences.aFilterString.length > 0);
		}
		if( typeof UserPreferences.aFilterString !== "undefined" ) {
			oView.byId("vsdFilterLabel").setText(UserPreferences.aFilterString);
		}
		//Openitem
		this.mGroupFunctions = {
			Openitem: function(oContext) {
				var name = oContext.getProperty("Openitem");
				return {
					key: name,
					text: name
				};
			},
			AddInfos: function(oContext) {
				var name = oContext.getProperty("AddInfos");
				return {
					key: name,
					text: name
				};
			}
		};
	},
	handleViewSettingsDialogButtonPressed: function(oEvent) {
		if (!this._oDialog) {
			this._oDialog = sap.ui.xmlfragment("com.springer.financefscmapp.view.HelpDialogs.Dialog_InvItems", this);
		}
		// toggle compact style
		jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialog);
		this._oDialog.open();
	},
	handleConfirmInvList: function(oEvent) {
		var oView    = this.getView();
		var mParams  = oEvent.getParameters();

		// apply sorter to binding
		// (grouping comes before sorting)
		var sPath;
		var bDescending;
		var aSorters = [];
		var aFilters = [];

		/*  GROUP  */
		if (mParams.groupItem) {
			console.log("Group Items");
			sPath = mParams.groupItem.getKey();
			bDescending = mParams.groupDescending;
			var vGroup = this.mGroupFunctions[sPath];
			aSorters.push(new sap.ui.model.Sorter(sPath, bDescending, vGroup));
		}

		/*  SORT  */
		if (mParams.sortItem) {
			console.log("Sort Items");
			sPath = mParams.sortItem.getKey();
			bDescending = mParams.sortDescending;
			aSorters = [];
			aSorters.push(new sap.ui.model.Sorter(sPath, bDescending));
		}

		/*  FILTER  */
		if (mParams.filterItems) {
			console.log("Filter Items");
			// apply filters to binding
			jQuery.each(mParams.filterItems, function(i, oItem) {
				var aSplit = oItem.getKey().split("___");
				var sColumn = aSplit[0];
				var sOperator = aSplit[1];
				var sValue = aSplit[2];
				var sSelectionOperator;

				switch (sOperator) {
					case "EQ":
						sSelectionOperator = sap.ui.model.FilterOperator.EQ;
						break;
					case "NE":
						sSelectionOperator = sap.ui.model.FilterOperator.NE;
						break;
					case "CS":
						sSelectionOperator = sap.ui.model.FilterOperator.Contains;
						break;
					case "GE":
						sSelectionOperator = sap.ui.model.FilterOperator.GE;
						break;
					case "GT":
						sSelectionOperator = sap.ui.model.FilterOperator.GT;
						break;
					case "LE":
						sSelectionOperator = sap.ui.model.FilterOperator.LE;
						break;
					case "LT":
						sSelectionOperator = sap.ui.model.FilterOperator.LT;
						break;
						
					//case "BT":
					//   sap.ui.model.FilterOperator.BT
					//   break;
				}
				var oFilter;
				oFilter = new sap.ui.model.Filter(sColumn, sSelectionOperator, sValue);
				aFilters.push(oFilter);
			});
		}
		
		var UserPreferences = sap.ui.getCore().getModel("UserPreferences");
		var oTable   = this.getView().byId("idInvListTable");
		oTable.unbindAggregation("items");
		oTable.bindAggregation("items", {
							path     : UserPreferences.oldInvoiceAggregationPath, 
							template : sap.ui.xmlfragment("com.springer.financefscmapp.view.HelpDialogs.RowFiDocument", this),
							sorter   : aSorters,
							filters  : aFilters
						}
					);

		// save filter and sorter 
		UserPreferences.aFiltersInv = aFilters;
		UserPreferences.aSortersInv = aSorters;
		UserPreferences.aFilterString = mParams.filterString;
		sap.ui.getCore().setModel(UserPreferences, "UserPreferences");
		
		// update filter bar
		oView.byId("vsdFilterBar").setVisible(aFilters.length > 0);
		oView.byId("vsdFilterLabel").setText(mParams.filterString);
	},
	handleSelectionChange: function(oEvent) {
		var partner = com.springer.financefscmapp.util.Formatter.overlayTenZero(oEvent.getSource().getBindingContext().getProperty("Partner"));
		sap.ui.controller("com.springer.financefscmapp.view.SecondLevel.BPInvoiceList").handleSelectionChange(oEvent, this, partner);
	},
	refreshData: function() {
		console.log("refresh");
		var model = this.getView().getModel();
		var UserPreferences = sap.ui.getCore().getModel("UserPreferences");
		if (UserPreferences.AppView === "OpenItemLive") {
			model.refresh();
		} else {
			if (com.springer.financefscmapp.dev.devapp.isLoaded) {
				if (UserPreferences.onlineStatus) {
					var oEventBus = this.getEventBus();
					oEventBus.publish("OfflineStore", "Refreshing");
				} else {
					//var filters = [];
					//this.getView().byId("list").getBinding("items").filter(filters);
					model.refresh();
				}
			} else {
				//var filters = [];
				//this.getView().byId("list").getBinding("items").filter(filters);
				model.refresh();
			}
		}
	},

	onInvoiceSearch: function() {
		var oView = this.getView();
		var UserPreferences = sap.ui.getCore().getModel("UserPreferences");
		var aSorters = [];
		if ( UserPreferences.aSortersInv && UserPreferences.aSortersInv.length > 0  ) {
			aSorters = UserPreferences.aSortersInv;
		}
		var filters = [];
		var searchString = oView.byId("InvoiceSearchText").getValue();
		console.log("SearchInvoice: "+searchString);
		if (searchString && searchString.length > 0) {
			var filter1 = new sap.ui.model.Filter("SapDocumentId", sap.ui.model.FilterOperator.EQ, searchString);
			//var filter2 = new sap.ui.model.Filter("PartnerDesc", sap.ui.model.FilterOperator.Contains, searchString);
			filters = [filter1];
		} else if ( typeof this.filterOImodeActive !== "undefined") {
			filters = [this.filterOImodeActive];
		}
		
		if (filters.length === 0 ) {
			if ( UserPreferences.aFiltersInv && UserPreferences.aFiltersInv.length > 0  ) {
				filters = UserPreferences.aFiltersInv;
			} else {
				filters = [];
			}
		}
		// update list binding
		console.log("Update List Binding");
		var oTable   = oView.byId("idInvListTable");
		oTable.unbindAggregation("items");
		oTable.bindAggregation("items", {
							path     : UserPreferences.oldInvoiceAggregationPath, 
							template : sap.ui.xmlfragment("com.springer.financefscmapp.view.HelpDialogs.RowFiDocument", this),
							sorter   : aSorters,
							filters  : filters
						}
					);
		UserPreferences.aFiltersInv = filters;
		sap.ui.getCore().setModel(UserPreferences, "UserPreferences");
		if (searchString && searchString.length > 0) {
			oView.byId("vsdFilterBar").setVisible(true);
			oView.byId("vsdFilterLabel").setText(searchString);
		}
	},
	onNavBack: function() {
		// nav button only visible for phone mode
		var UserPreferences = sap.ui.getCore().getModel("UserPreferences");
		if(UserPreferences.PrefereListMode !== "X") {
			this.getRouter().myNavBack("BPOverviewMD_Master");
		} else {
			if(this.navTwoSteps === true) {
				window.history.go(-2);
			} else {
				this.getRouter().myNavBack("BPOverviewMD_Master");
				//window.history.go(-1);
			}
		}
	},
	handleNavToHome: function() {
		var bReplace = sap.ui.Device.system.phone ? false : true;
		this.getRouter().navTo("_A2_Welcome", {
			currentView: this.getView()
		}, bReplace);
	}

});