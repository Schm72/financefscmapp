jQuery.sap.require("com.springer.financefscmapp.util.Formatter");
jQuery.sap.require("com.springer.financefscmapp.util.Controller");

com.springer.financefscmapp.util.Controller.extend("com.springer.financefscmapp.view.SecondLevel.BPOverviewList", {

	_oDialog: null,
	showTexts: false,
	filterOImodeA: new sap.ui.model.Filter("AppFilter", sap.ui.model.FilterOperator.EQ, "X"),
	filterOImodeB: null,
	filterOImodeActive: {},
	UserPreferences: {},
	_vUserIdWLmode: {},
	_vListViewChanger: {},
	_vListDrpd: {},
	activeHeaderToolbar: {},
	i18model: {},
	_oTable: {},

	onInit: function() {
		this.UserPreferences = sap.ui.getCore().getModel("UserPreferences");
		if(typeof this.UserPreferences === "undefined") {
			console.log("Redirect to start");
			this.getRouter().navTo("_A1_firstVisitCheck", {
				currentView: this.getView()
			}, false);
			return;
		}

		this.getView().addEventDelegate({
			onAfterShow: jQuery.proxy(function(evt) {
				this.onAfterShow(evt);
			}, this)
		});
		this._oTable = this.getView().byId("idOIListTable");
		this._vUserIdWLmode = this.getView().byId("UserIdWLmode");
		this._vListViewChanger = this.getView().byId("oiListViewChanger");
		this._vListDrpd = this.getView().byId("ioListDrpd");
		this.getRouter().attachRoutePatternMatched(this.onRouteMatched, this);
		if (this.UserPreferences.device === "Desktop" || this.UserPreferences.device === "undefined" || this.UserPreferences.device.indexOf(
			"Tablet") > -1 || sap.ui.Device.orientation.landscape) {
			this.showTexts = true;
		}
	},
	waitForInitialListLoading: function(fnToExecute) {
		jQuery.when(this.oInitialLoadFinishedDeferred).then(jQuery.proxy(fnToExecute, this));
	},
	onRouteMatched: function(evt) {
		// check that the called view name is like expected
		if (evt.getParameter("name") !== "_BPOverviewList") {
			return;
		}
		this.i18model = this.getView().getModel("i18n").getResourceBundle();

		// get current userPreference object
		this.UserPreferences = sap.ui.getCore().getModel("UserPreferences");

		var sAggregationPath = "";
		if (this.UserPreferences.AppView === "OpenItemLive") {
			sAggregationPath = "/OPEN_ITEM_BP_OVERVIEWSet";
		} else {
			sAggregationPath = "/OPEN_ITEMS_SAVED_PER_USERSet";
		}

		var aFilters = [];
		var aSorters = [];
		if (this.UserPreferences.aFiltersOvv && this.UserPreferences.aFiltersOvv.length > 0) {
			aFilters = this.UserPreferences.aFiltersOvv;
		}
		if (this.UserPreferences.aSortersOvv && this.UserPreferences.aSortersOvv.length > 0) {
			aSorters = this.UserPreferences.aSortersOvv;
		} else if (!this.UserPreferences.onlineStatus) {
			aSorters.push(new sap.ui.model.Sorter("FscmPrio", true));
		}
		// check if we need to rebind the aggregation (when live / fav mode changed OR when list View / master detail view change)
		if (typeof this.UserPreferences.oldListEnteredOIListView === "undefined" || this.UserPreferences.oldListEnteredOIListView !== this.UserPreferences
			.enteredOIListView ||
			typeof this.UserPreferences.oldListAggregationPath === "undefined" || this.UserPreferences.oldListAggregationPath !== sAggregationPath) {
			console.log("===BIND CHANGE===");
			this.UserPreferences.oldListAggregationPath = sAggregationPath;
			this.UserPreferences.oldListEnteredOIListView = this.UserPreferences.enteredOIListView;
			sap.ui.getCore().setModel(this.UserPreferences, "UserPreferences");
			this._oTable.unbindAggregation("items");
			this._oTable.bindAggregation("items", {
				path: sAggregationPath,
				template: sap.ui.xmlfragment("com.springer.financefscmapp.view.HelpDialogs.OpenItemPartnerListRows", this),
				sorter: aSorters,
				filters: aFilters
			});

			this.oInitialLoadFinishedDeferred = jQuery.Deferred();
			this._oTable.attachEventOnce("updateFinished", function() {
				this.oInitialLoadFinishedDeferred.resolve();
			}, this);
			this.waitForInitialListLoading(function() {
				this.checkNoDataDetails();
			});
		} else {
			this.getView().setBusy(false);
			console.log("No BInding change");
			// check if special filter sort active active 
			/*if (aSorters.length > 0  || aFilters.length > 0 ) {
				var oBinding = this._oTable.getBinding("items");
			}
			if (aSorters.length > 0 ) {
				oBinding.sort(aSorters);
			}
			if (aFilters.length > 0 ) {
				oBinding.filter(aFilters);
			}*/
		}

		this.mGroupFunctions = {
			DeliveryBlock: function(oContext) {
				var name = oContext.getProperty("DeliveryBlock");
				return {
					key: name,
					text: name
				};
			}
		};
	},

	onAfterShow: function() {
		console.log("Entered: BPOverviewList");

		if (this.UserPreferences.AppView === "OpenItemLive") {
			console.log("live");
			this.getView().byId("invoiceDetailsBP").setTitle("Partner Worklist Overview");
			this.getView().byId("addFavButton").setVisible(false);
		} else {
			console.log("fav");
			this.getView().byId("invoiceDetailsBP").setTitle("Favorites Worklist Overview");
			this.getView().byId("addFavButton").setVisible(true);
		}

		// parameter which View is active
		if (this.UserPreferences.vVisibleOpenItemEnhanced === false) {
			this._vListViewChanger.setIcon("sap-icon://drill-down");
		} else {
			this._vListViewChanger.setIcon("sap-icon://drill-up");
		}
		// check text view on/off
		if (this.showTexts === true) {
			if (this.UserPreferences.vVisibleOpenItemEnhanced === false) {
				this._vListViewChanger.setText(this.i18model.getText("OIExtendView"));
			} else {
				this._vListViewChanger.setText(this.i18model.getText("OISlimView"));
			}
		} else {
			this._vListViewChanger.setText("");
			this._vUserIdWLmode.setText("");
		}
		// style class depends on view mode
		if (this.UserPreferences.vVisibleOpenItemEnhanced === false || document.body.clientWidth > 9001) {
			this._oTable.removeStyleClass("classStripColor");
			this._oTable.addStyleClass("classEvery2Color");
		} else {
			this._oTable.removeStyleClass("classEvery2Color");
			this._oTable.addStyleClass("classStripColor");
		}

		// at the first time view is collapsed
		if (this.UserPreferences.enteredOIListView === undefined) {
			this.UserPreferences.enteredOIListView = true;
			this.UserPreferences.vVisibleOpenItemEnhanced = false;
			this._vListViewChanger.setIcon("sap-icon://drill-down");
			if (this.showTexts === true) {
				this._vListViewChanger.setText(this.i18model.getText("OIExtendView"));
			}
			if (this.UserPreferences.vVisibleOpenItemEnhanced === false || document.body.clientWidth > 9001) {
				this._oTable.removeStyleClass("classStripColor");
				this._oTable.addStyleClass("classEvery2Color");
			} else {
				this._oTable.removeStyleClass("classEvery2Color");
				this._oTable.addStyleClass("classStripColor");
			}
		}

		// check filter item mode
		switch (this.UserPreferences.PreferedOiFilter) {
			case "X":
				this.filterOImodeActive = this.filterOImodeA;
				this._vUserIdWLmode.setText(this.i18model.getText("OIOnlyOpenItems"));
				this._vUserIdWLmode.setIcon("sap-icon://collections-insight");
				break;
			default:
				this.filterOImodeActive = this.filterOImodeB;
				this._vUserIdWLmode.setText(this.i18model.getText("OIAllInFilter"));
				this._vUserIdWLmode.setIcon("sap-icon://add-activity");
		}

	},

	checkNoDataDetails: function() {
		var items = this._oTable.getItems();
		// when there are no items - check wether there is a background load running - or just no data with current filter
		var oModel = this.getView().getModel();
		var that = this;

		if (items.length < 1) {
			//if (that._oTable.getBinding("items").getLength() < 1) {
			oModel.read("/APP_OI_USER_LOGSet('" + this.UserPreferences.UserId + "')", null, null, true,
				function(oData) {
					that.getView().setBusy(false);
					if (oData) {
						if (that.UserPreferences.AppView === "OpenItemLive") {
							that._oTable.setNoDataText(that.i18model.getText("OIListNoDataText"));
						} else {
							that._oTable.setNoDataText(that.i18model.getText("OINoDataFilterFav"));
						}
					} else {
						that._oTable.setNoDataText(that.i18model.getText("OINoDataSuccess"));
					}
				},
				function(oError) {
					that.getView().setBusy(false);
					that._oTable.setNoDataText(that.i18model.getText("OINoServerCon"));
				}
			);
		} else {
			// read first row
			var data = oModel.getProperty(items[0].getBindingContext().getPath());
			// check property of first row
			that.getView().setBusy(false);
		}
	},

	onItemSearch1: function(oControlEvent) {
		// add filter for search
		this.getView().setBusy(true);
		var filters = [];
		var searchString = this.getView().byId("OpenItemSearchText1").getValue();
		if (searchString && searchString.length > 0) {
			filters = [new sap.ui.model.Filter("Partner", sap.ui.model.FilterOperator.EQ, searchString)];
		} else if (this.filterOImodeActive !== null) {
			filters = [this.filterOImodeActive];
		}

		// update list binding
		this.getView().byId("idOIListTable").getBinding("items").filter(filters);

		this.checkNoDataDetails();
	},
	onItemSearch2: function() {
		// add filter for search
		this.getView().setBusy(true);
		var filters = [];
		var searchString = this.getView().byId("OpenItemSearchText2").getValue();
		if (searchString && searchString.length > 0) {
			filters = [new sap.ui.model.Filter("Lid", sap.ui.model.FilterOperator.EQ, searchString)];
		} else if (this.filterOImodeActive !== null) {
			filters = [this.filterOImodeActive];
		}

		// update list binding
		this.getView().byId("idOIListTable").getBinding("items").filter(filters);
		this.getView().setBusy(false);
	},

	handleViewSettingsDialogButtonPressed: function(oEvent) {
		if (!this._oDialog) {
			this._oDialog = sap.ui.xmlfragment("com.springer.financefscmapp.view.HelpDialogs.Dialog_OpenItems", this);
		}
		// toggle compact style
		jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialog);
		this._oDialog.open();
	},

	handleConfirm: function(oEvent) {

		var oView = this.getView();
		var mParams = oEvent.getParameters();

		// apply sorter to binding
		// (grouping comes before sorting)
		var sPath;
		var bDescending;
		var aFilters = [];
		var aSorters = [];

		/*  GROUP  */
		if (mParams.groupItem) {
			sPath = mParams.groupItem.getKey();
			bDescending = mParams.groupDescending;
			var vGroup = this.mGroupFunctions[sPath];
			aSorters.push(new sap.ui.model.Sorter(sPath, bDescending, vGroup));
		}

		/*  SORT  */
		if (mParams.sortItem) {
			sPath = mParams.sortItem.getKey();
			bDescending = mParams.sortDescending;
			aSorters.push(new sap.ui.model.Sorter(sPath, bDescending));
		}

		/*  FILTER  */
		if (mParams.filterItems) {
			// apply filters to binding
			jQuery.each(mParams.filterItems, function(i, oItem) {
				var key = oItem.getKey();
				var oFilter;
				oFilter = new sap.ui.model.Filter(key, sap.ui.model.FilterOperator.GT, 0);
				aFilters.push(oFilter);
			});
		}

		this.UserPreferences = sap.ui.getCore().getModel("UserPreferences");
		this._oTable.unbindAggregation("items");
		this._oTable.bindAggregation("items", {
			path: this.UserPreferences.oldListAggregationPath,
			template: sap.ui.xmlfragment("com.springer.financefscmapp.view.HelpDialogs.OpenItemPartnerListRows", this),
			sorter: aSorters,
			filters: aFilters
		});

		// update filter bar
		oView.byId("vsdFilterBar").setVisible(aFilters.length > 0);
		oView.byId("vsdFilterLabel").setText(mParams.filterString);

		// save filter and sorter 
		this.UserPreferences.aFiltersOvv = aFilters;
		this.UserPreferences.aSortersOvv = aSorters;
		sap.ui.getCore().setModel(this.UserPreferences, "UserPreferences");
	},
	onDetailView: function() {
		var bReplace = sap.ui.Device.system.phone ? false : true;
		this.getRouter().navTo("_BPOverviewMD_Master", {
			currentView: this.getView()
		}, bReplace);
	},

	handleViewChangeTop: function() {
		var vId = null;
		this.UserPreferences.vVisibleOpenItemEnhanced = true;
		var Columns = this._oTable.getColumns();
		for (var i = 0; i < Columns.length; i++) {
			vId = Columns[i].getId();
			if (vId.indexOf("idHiddenColmn") > -1) {
				if (Columns[i].getVisible() === true) {
					Columns[i].setVisible(false);
					this.UserPreferences.vVisibleOpenItemEnhanced = false;
					this._vListViewChanger.setIcon("sap-icon://drill-down");
					if (this.showTexts === true) {
						this._vListViewChanger.setText(this.i18model.getText("OIExtendView"));
					}
				} else {
					Columns[i].setVisible(true);
					this.UserPreferences.vVisibleOpenItemEnhanced = true;
					this._vListViewChanger.setIcon("sap-icon://drill-up");
					if (this.showTexts === true) {
						this._vListViewChanger.setText(this.i18model.getText("OISlimView"));
					}
				}
			}
		}
		if (this.UserPreferences.vVisibleOpenItemEnhanced === false || document.body.clientWidth > 9001) {
			this._oTable.removeStyleClass("classStripColor");
			this._oTable.addStyleClass("classEvery2Color");
		} else {
			this._oTable.removeStyleClass("classEvery2Color");
			this._oTable.addStyleClass("classStripColor");
		}

		sap.ui.getCore().setModel(this.UserPreferences, "UserPreferences");
	},

	handleChangeOImode: function() {

		this.getView().setBusy(true);
		switch (this.UserPreferences.PreferedOiFilter) {
			case "X":
				this.filterOImodeActive = this.filterOImodeB;
				this._vUserIdWLmode.setText(this.i18model.getText("OIAllInFilter"));
				this._vUserIdWLmode.setIcon("sap-icon://add-activity");
				this.UserPreferences.PreferedOiFilter = "";
				break;
			default:
				this.filterOImodeActive = this.filterOImodeA;
				this._vUserIdWLmode.setText(this.i18model.getText("OIOnlyOpenItems"));
				this._vUserIdWLmode.setIcon("sap-icon://collections-insight");
				this.UserPreferences.PreferedOiFilter = "X";
		}
		sap.ui.getCore().setModel(this.UserPreferences, "UserPreferences");

		var filters = [];
		if (this.filterOImodeActive !== null) {
			filters = [this.filterOImodeActive];
		}
		this._oTable.getBinding("items").filter(filters);

		var that = this;
		var oModel = this.getView().getModel();
		oModel.read("/APP_USER_PREFERENCESSet('" + this.UserPreferences.UserId + "')", null, null, true,
			function(oData, oResponse) {
				oData.PreferedOiFilter = that.UserPreferences.PreferedOiFilter;
				oModel.update("/APP_USER_PREFERENCESSet('" + that.UserPreferences.UserId + "')", oData, null, null, null);
				that.getView().setBusy(false);
				that.refreshData();
			},
			function(oError) {
				that.getView().setBusy(false);
			}
		);
	},

	onAddFav: function(oEvent) {
		this.getView().setBusy(true);
		this.addedFavorite = false;
		if (oEvent.getParameter("state") === true) {
			this.addedFavorite = true;
		}

		this.currentObinding = oEvent.getSource().getBindingContext();
		this.currentFavoritePartner = com.springer.financefscmapp.util.Formatter.overlayTenZero(this.currentObinding.getProperty("Partner"));

		var oModel = this.getView().getModel();
		var UserPreferences = sap.ui.getCore().getModel("UserPreferences");
		// change the favorite information
		if (UserPreferences.AppView === "OpenItemLive" || this.addedFavorite === true) {
			this.addRemoveFavorite(this.addedFavorite, oModel, "OPEN_ITEM_BP_OVERVIEWSet('" + this.currentFavoritePartner + "')", this.currentFavoritePartner,
				this);
		} else {
			this.openDeleteConfirmDialog();
		}
	},

	// Take care of the navigation through the hierarchy when the user selects a table row
	handleSelectionChange: function(oEvent) {

		this.getView().setBusy(true);
		var oItem = oEvent.getSource();

		var contextModel = {};
		contextModel = sap.ui.getCore().getModel("ContextModel");
		if (typeof contextModel === "undefined") {
			contextModel = new sap.ui.model.json.JSONModel();
		}
		contextModel.partner = oItem.getBindingContext().getProperty("Partner");
		sap.ui.getCore().setModel(contextModel, "ContextModel");

		var bReplace = sap.ui.Device.system.phone ? false : true;
		this.getRouter().navTo("_BPInvoiceList", {
			from: "_BPOverviewList",
			entity: oItem.getBindingContext().getPath().substr(1)
		}, bReplace);
	},

	refreshData: function() {
		var filters = [];
		var searchString = this.getView().byId("OpenItemSearchText1").getValue();
		if (searchString && searchString.length > 0) {
			filters = [new sap.ui.model.Filter("Partner", sap.ui.model.FilterOperator.EQ, searchString)];
		} else if (this.filterOImodeActive !== null) {
			filters = [this.filterOImodeActive];
		}
		// update list binding
		this.getView().byId("idOIListTable").getBinding("items").filter(filters);

		var UserPreferences = sap.ui.getCore().getModel("UserPreferences");
		if (UserPreferences.onlineStatus && com.springer.financefscmapp.dev.devapp.isLoaded) {
			var oEventBus = this.getEventBus();
			oEventBus.publish("OfflineStore", "Refreshing");
		}
	},

	openDialog: function(sI18nKeyword) {
		if (!this._dialog) {
			var id = this.getView().getId();
			var frgId = id + "-msgDialog";
			this._dialog = sap.ui.xmlfragment(frgId, "com.springer.financefscmapp.view.HelpDialogs.MsgDialog", this);
			this.getView().addDependent(this._dialog);
			this._dialogText = sap.ui.core.Fragment.byId(frgId, "dialogText");
		}

		this._dialogText.bindProperty("text", sI18nKeyword);
		this._dialog.open();
	},
	closeDialog: function() {
		this.getView().setBusy(false);
		if (this._dialog) {
			this._dialog.close();
		}
	},
	openDeleteConfirmDialog: function() {
		console.log("deleteDialogStart");
		if (!this._deleteConfirmDialog) {
			var id = this.getView().getId();
			var frgId = id + "-_dialog_DeleteConfirm";
			this._deleteConfirmDialog = sap.ui.xmlfragment(frgId, "com.springer.financefscmapp.view.HelpDialogs.Dialog_DeleteConfirm", this);
			this.getView().addDependent(this._deleteConfirmDialog);
		}
		this._deleteConfirmDialog.open();
	},

	confirmDelete: function() {
		if (this._deleteConfirmDialog) {
			this._deleteConfirmDialog.close();
		}
		if (this.currentFavoritePartner !== "") {
			var UserPreferences = sap.ui.getCore().getModel("UserPreferences");
			var delPath = "/OPEN_ITEMS_SAVED_PER_USERSet(UserId='" + UserPreferences.UserId + "',Partner='" + this.currentFavoritePartner +
				"')";
			var model = this.getView().getModel();
			if (model) {
				this.getView().setBusy(true);
				model.remove(delPath, {
					success: jQuery.proxy(function() {
						this.getView().setBusy(false);
						sap.m.MessageToast.show(this.i18model.getText("OIRemovedFav") + " " + this.currentFavoritePartner);
						UserPreferences.CountFscmFav--;
						sap.ui.getCore().setModel(UserPreferences, "UserPreferences");
					}, this),
					error: jQuery.proxy(function() {
						this.getView().setBusy(false);
						sap.m.MessageToast.show("communication error");
					}, this)
				});
			}
		}
	},
	closeDeleteConfirmDialog: function() {
		if (!this.addedFavorite && typeof this.currentObinding !== "undefined") {
			this.currentObinding.getModel().setProperty("AddedFavorite", true, this.currentObinding);
		}
		this.getView().setBusy(false);
		if (this._deleteConfirmDialog) {
			this._deleteConfirmDialog.close();
		}
	},
	onAddFavorite: function() {
		this.onAddFavoriteController(this);
	},
	onNavBack: function() {
		var bReplace = sap.ui.Device.system.phone ? false : true;
		this.getRouter().navTo("_A2_Welcome", {
			currentView: this.getView()
		}, bReplace);
		//window.history.go(-1);
	},
	handleNavToHome: function() {
		var bReplace = sap.ui.Device.system.phone ? false : true;
		this.getRouter().navTo("_A2_Welcome", {
			currentView: this.getView()
		}, bReplace);
	}
});