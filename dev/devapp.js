jQuery.sap.declare("com.springer.financefscmapp.dev.devapp");
jQuery.sap.require("com.springer.financefscmapp.dev.devlogon");

com.springer.financefscmapp.dev.devapp = {
	smpInfo: {},
	entityName: null,
	navName: null,
	isLoaded: false,
	externalURL: null,
	isOnline: false,
	
	//Application Constructor
	initialize: function() {
		var s = "{&quot;APP_MESSAGESSet&quot;:&quot;/APP_MESSAGESSet&quot;,&quot;APP_USER_PREFERENCESSet&quot;:&quot;/APP_USER_PREFERENCESSet&quot;,&quot;ATTACHMENT_INFO_LIST_SVDSet&quot;:&quot;/ATTACHMENT_INFO_LIST_SVDSet&quot;,&quot;OPEN_ITEMS_SAVED_INVOICES_PER_USERSet&quot;:&quot;/OPEN_ITEMS_SAVED_INVOICES_PER_USERSet&quot;,&quot;OPEN_ITEMS_SAVED_PER_USERSet&quot;:&quot;/OPEN_ITEMS_SAVED_PER_USERSet&quot;}";
		if (s && s.length > 0) {
			var ps = $("<div/>").html(s).text();
			this.definedStore = JSON.parse(ps);
		}
		this.entityName = "OPEN_ITEM_BP_OVERVIEWSet";
		this.bindEvents();
	},

	//========================================================================
	// Bind Event Listeners
	//========================================================================
	bindEvents: function() {
		//add an event listener for the Cordova deviceReady event.
		document.addEventListener("deviceready", this.onDeviceReady, false);
		document.addEventListener("online", this.deviceOnline, false);
		document.addEventListener("offline", this.deviceOffline, false);
		//document.addEventListener("pause", this.onPause, false);
		//document.addEventListener("resume", this.onResume, false);
	},
	
	//========================================================================
	//Cordova Device Ready
	//========================================================================
	onDeviceReady: function() {
		console.log("onDeviceReady");
		if (window.sap_webide_FacadePreview) {
			console.log("=====WebIDE preview=====");
			startApp();
		} else {
			var that = com.springer.financefscmapp.dev.devapp;
			var boolAppContext = false;
			if (sap.ui.Device.system.tablet || sap.ui.Device.system.phone) {
				boolAppContext = true;
				that.isOnline = true;
			}
			if (window.cordova && window.cordova.platformId !== "browser" && boolAppContext) {
				$.getJSON(".project.json", function(data) {
					if (data && data.hybrid && data.hybrid.plugins.kapsel.logon.selected) {
						that.smpInfo.server = data.hybrid.msType === 0 ? data.hybrid.hcpmsServer : data.hybrid.server;
						that.smpInfo.port = data.hybrid.msType === 0 ? "443" : data.hybrid.port;
						that.smpInfo.appID = data.hybrid.appid;
	
						//external Odata service url
						if (data.hybrid.externalURL && data.hybrid.externalURL.length > 0) {
							that.externalURL = data.hybrid.externalURL;
						}
					}
	
					if (that.smpInfo.server && that.smpInfo.server.length > 0) {
						var context = {
							"serverHost": that.smpInfo.server,
							//"https": data.hybrid.msType === 0 ? "true" : "false",
							"https": "true",
							"serverPort": that.smpInfo.port
						};
						that.devLogon = new com.springer.financefscmapp.dev.devlogon();
						that.devLogon.doLogonInit(context, that.smpInfo.appID, that.entityName, that.navName);
						console.log("=====SMP start=======");
					} else {
						console.log("=====WebbApp1=====");
						startApp();
					}
				});
			} else {
				console.log("=====WebbApp2=====");
				startApp();
			}
		}
	},


	//========================================================================
	//Cordova deviceOnline event handler
	//========================================================================
	deviceOnline: function() {
		console.log("deviceOnline");
		var that = com.springer.financefscmapp.dev.devapp;
		//console.log("Device is Online", { duration: 1000});
		if (that.isLoaded && that.deviceModel) {
			that.deviceModel.setProperty("/isOffline", false);
		}
		that.isOnline = true;
	},

	//========================================================================
	//Cordova deviceOffline event handler
	//========================================================================
	deviceOffline: function() {
		console.log("deviceOffline");
		var that = com.springer.financefscmapp.dev.devapp;
		//console.log("Device is Offline", { duration: 1000});
		if (that.isLoaded && that.deviceModel) {
			that.deviceModel.setProperty("/isOffline", true);
		}
		that.isOnline = false;
	}
};